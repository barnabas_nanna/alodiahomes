/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$('.save-property').click(function(){
    var propertyId = $(this).attr('data-propertyid');
    var self = $(this);
    if(propertyId)
    {
       $.ajax({
           url : '/property/saveproperty',
           type : 'POST',
           dataType : 'json',
           data : {
               propertyId : propertyId
           },
           success : function(data)
           {
               if(data.error)
               {
                   Alertify.displayError(data.message);
                   
               }else
               {
                  
                  self.find('span').text('Property Saved');
                  self.unbind('click');//remove click event
                  Alertify.displaySuccess(data.message);
               }
           }
       });
    }
});

$('.email-landlord').click(function(){
    var propertyId = $(this).attr('data-propertyid');
    var self = $(this);
    if(propertyId)
    {
       location.href = '/messages/new/?propertyid='+propertyId
    }
});

var Alertify = {
    displayError : function(message)
    {
        alert(message);
    },
    displaySuccess : function(message)
    {
        alert(message);
    }
};