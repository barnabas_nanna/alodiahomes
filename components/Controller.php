<?php
namespace app\components;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Controller
 *
 * @author BarnabasU
 */
class Controller extends \yii\web\Controller {
    
    public $defaultAdminActions = ['index','view','update','create','delete'];
    public $layout = "/column2";
    
    
    public function beforeAction($action)
    {
       if(property_exists($this, 'adminActions'))
       {
           $this->defaultAdminActions = array_merge($this->defaultAdminActions , (array)$this->adminActions);
       }
              
       if(in_array($action->id, $this->defaultAdminActions)){
           
          if(!isAdmin())
          {
              return false;
          }
          
          $this->layout = '\column1';
       }
            
       
       return parent::beforeAction($action);
    }
}
