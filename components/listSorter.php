<?php
namespace app\components;

/**
 * Description of listSorter
 *
 * @author Barnabas
 */
class listSorter {
    
    public  $items = [];
    public $maxRows = 4;
    public $amountOfItems =0;
    public $displayTypes = ['list','table'];
    public $displayFormat = 'list';
    public $remainder = 0;
    public $itemsPerColumns = [];
    public $html = '';
    public $testData = [];
    public $errorMessage = 'None found';
    public $templatePath = '';


    public function __construct(array $items = [] , $maxColumns = 4, $displayFormat = 'list')
    {
        
        $this->items = !empty($items) ? $items : array_fill(0,1,lang($this->errorMessage));
        $this->maxRows = ($maxColumns > 4) ? 4 : $maxColumns;
        $this->amountOfItems = count($this->items);
        $this->itemsPerColumns = $this->setItemsPerColumn();
        $this->displayFormat = $displayFormat;
    }
    
    private function setItemsPerColumn()
    {
        $this->remainder = $this->amountOfItems%$this->maxRows;
        return $this->getIteration();
    }
    
    public function getHtml()
    {
        $r = $this->items;
                       
        for($i=0; $i < $this->maxRows; $i++ )
        {
            $length = $this->itemsPerColumns[$i];
            $start = ($i > 0 ) ? $this->itemsPerColumns[$i-1] : 0;
            $extractedItems = array_slice($r, $start , $length);
            $this->html .= $this->addWrapper($extractedItems);
        }
               
        return $this->html;
    }
    
    /**
     * Convert all items to string
     * @param array $items
     * @return array An array containing the string value of all items
     */
    protected function preFormat(array $items = [])
    {
        $i = [];
        foreach ($items as $value)
        {
            if(is_object($value))
            {
                $value = $value->toDisplayString();
            }
            
            if(is_string($value))
            {
                $i[] = $value;
            }
        }
        
        return $i;
    }


    protected function addWrapper(array $items = [])
    {
        
        $glue = ($this->getDisplayType() == 'table') ? '</td></tr><tr><td>' : '</li><li>';
        $string = join($glue, $this->preFormat($items));
        $template = $this->getTemplate();
        return $this->addClosingWrapper(str_replace('{{string}}', $string, $template));
    }
    
    protected function addClosingWrapper($string)
    {
        $rows = $this->maxRows;
        switch($rows){
            case 1:
                $class = 'col-xs-12';
                break;
            case 2:
                $class = 'col-xs-6';
                break;
            case 3:
                $class = 'col-xs-4';
                break;
            case 4:
                $class = 'col-xs-3';
                break;
            default :
                $class = 'col-xs-12';
                break;
            
        }
        
        return '<div class="'.$class.'">'.$string.'</div>';

    }
    
    public function __toString() {
        return $this->getHtml();
    }


    protected function getTemplate()
    {
        $type = $this->getDisplayType();
        switch ($type) {
            case 'list':
                $template = '<ul><li>{{string}}</li></ul>';
                break;
            case 'table':
                $template = '<table><tr><td>{{string}}</td></tr></table>';
                break;
            default:
                 $template = '';
                break;
        }
                
        return $template;
    }
    
    protected function getDisplayType()
    {
        return in_array($this->displayFormat,$this->displayTypes) ? $this->displayFormat : 'list';
    }
    
    private function getIteration()
    {
        $muliple = ($this->amountOfItems - $this->remainder ) / $this->maxRows;
        $r = array_fill(0, $this->maxRows, $muliple);
        if ($this->remainder) {
            for ($i = 0; $i < $this->remainder; $i++) {
                $r[$i] = $r[$i] + 1;
            }
        }
        return $r;
    }
}
