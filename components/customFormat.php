<?php
namespace app\components;
use yii\i18n\Formatter;
/**
 * Description of Deleted
 *
 * @author Barnabas
 */
class customFormat extends Formatter {
      
    function asDeleted($value = '')
    {
        $array = ['No','Hidden','Yes'];
        $r = $array[ (int)$value];
        return \lang($r);
    }
}
