<?php
namespace app\components;

/**
 * Description of SearchWidget
 * Searck box
 * @author Barnabas
 */
class SearchWidget extends \yii\bootstrap\Widget{
    //put your code here
    
    public function init()
    {
        parent::init();
    }
    
    public function run()
    {
        $view ='search.php';
        return $this->render($view);
    }
}
