<?php
namespace app\components;

/**
 * Description of Propertysearch
 *
 * @author Barnabas
 */
class Propertysearch {


    public $geoPoints, $town, $criteria, $query, $PropertyType, $uploaded, $radius;

    public function rules() {
        return array(
            array('query,radius,PropertyType,town,uploaded', 'safe'),
        );
    }

    public function attributeLabels() {

        return array(
            'radius' => 'search radius',
            'query' => 'region / city / postcode',
            'town' => 'location'
        );
    }

    /**
     * @return array
     */
    public function getRadiusData() {
        return array(
            '0.25' => '1/4 Mile',
            '0.5' => '1/2 Mile',
            1 => '1 mile',
            3 => '3 miles',
            5 => '5 miles',
            10 => '10 miles',
            15 => '15 miles',
            20 => '20 miles',
            30 => '30 miles',
            40 => '40 miles',
        );
    }

    public function PropertyAuctionTypes($additional = null) {
        $types = array(
            'Residential Vacant Possession' => 'residential vacant possession',
            'Residential Investment' => 'residential investment',
            'Commercial Vacant Possession' => 'commercial vacant possession',
            'Commercial Investment' => 'commercial investment',
            'Land & Development Opportunities & Garages' => 'land & development opportunities & garages',
            'Ground Rents' => 'ground rents'
        );
        if (is_null($additional) OR !is_array($additional)) {
            $additional = array();
        }

        return array_merge($types, $additional);
    }

    public function searchCriteria($auctionDates2=null) {
        $criteria = new CDbCriteria();
        //if there is a search query set
        if($auctionDates2===null)
            $auctionDates2 = PropertyAuction::model()->nextAuctionDate();
        //$criteria->condition = 'ShowOnWeb = 1';
        if($auctionDates2){
        $auctionId = $auctionDates2['Auction_id'];
        $criteria->addCondition('auction_id='.$auctionId);
        }
        $criteria->order='LotNumber asc';
        if ($this->query) {
            $postcode = $this->getCoords($this->query);
            if ($postcode && count($postcode)) {
                $this->geoPoints = $postcode;
                if (isset($postcode['lat'])) {
                    
                } else {
                    return false;
                }
            }
        }

        if ($this->PropertyType) {
            $criteria->join = "INNER JOIN property_auction_lots_data pld ON pld.lot_id = t.lot_id";
            $criteria->addCondition("pld.name='Property Type'");
            if ($this->PropertyType != 'other')
                $criteria->addCondition("value='{$this->PropertyType}'");
            else
                $criteria->addCondition("value NOT IN ('Residential Vacant Possessions',
                   'Commercial Vacant Possession', 'Commercial Investment', 
                   'Land & Development Opportunities & Garages','Ground Rents', 'Residential Investment')");
        }

        if ($this->town) {
            $criteria->addCondition("Town='$this->town'");
        }

        $this->criteria = $criteria;

        return $this->replaceCondition();
    }

    public function boundary($criteria) {
        $withAreaPropertyIds = $this->withinArea($criteria);
        $sql = '';
        if ($criteria == false)
            return false;
        $ctoarray = $criteria->toArray();
        $store = $withAreaPropertyIds;
        if (isset($this->geoPoints['SW']) && isset($this->geoPoints['NE'])) {
            $array = calCenter($this->geoPoints['NE'], $this->geoPoints['SW']);
            if (count($array)) {
                foreach ($array as $key => $value) {

                    $select = "t.lot_id";
                    $addWhere = "( 3959 * acos( cos( radians('" . $value[0] . "') ) * cos( radians( t.latitude ) ) 
             * cos( radians( t.longitude ) - radians('" . $value[1] . "') ) + 
             sin( radians('" . $value[0] . "') ) * sin( radians( t.latitude ) ) ) ) <= " . $this->radius;

                    $command = Yii::app()->db->createCommand();
                    $command->select($select)->from('property_auction_lots as t')->
                            where($ctoarray['condition'])->andWhere($addWhere)->order($ctoarray['order'])
                            ->limit($ctoarray['limit']);
                    if ($ctoarray['join'] && strlen($ctoarray['join'])) {
                        $command->join('property_auction_lots_data pld', 'pld.lot_id = t.lot_id');
                    }

                    $sql = Yii::app()->db->createCommand($command->text)->queryAll();
                    foreach ($sql as $key => $value) {
                        if (!in_array($value['lot_id'], $store)) {
                            $store[] = $value['lot_id'];
                        }
                    }
                }
                if (count($store))
                    return $store;
            }
            return array();
        }
    }

    public function withinArea($criteria) {
        $sql = '';
        if ($criteria == false)
            return false;
        $ctoarray = $criteria->toArray();
        $store = array();
        if (isset($this->geoPoints['SW']) && isset($this->geoPoints['NE'])) {
            $array = calCenter($this->geoPoints['NE'], $this->geoPoints['SW']);
            if (count($array)) {
                $x1 = $this->geoPoints['NE']['lat'];
                $y1 = $this->geoPoints['NE']['lng'];
                $x2 = $this->geoPoints['SW']['lat'];
                $y2 = $this->geoPoints['SW']['lng'];
                $select = "t.lot_id";
                $addWhere = "(($x2 <= t.latitude) AND
            (t.latitude <= $x1)) AND (($y2 <= t.longitude) AND (t.longitude <= $y1))";

                $command = Yii::app()->db->createCommand();
                $command->select($select)->from('property_auction_lots as t')->
                        where($ctoarray['condition'])->andWhere($addWhere)->order($ctoarray['order'])
                        ->limit($ctoarray['limit']);
                if ($ctoarray['join'] && strlen($ctoarray['join'])) {
                    $command->join('property_auction_lots_data pld', 'pld.lot_id = t.lot_id');
                }

                $sql = Yii::app()->db->createCommand($command->text)->queryAll();
                foreach ($sql as $key => $value) {
                    if (!in_array($value['lot_id'], $store)) {
                        $store[] = $value['lot_id'];
                    }
                }

                if (count($store))
                    return $this->getPropertiesWithoutGeo($store);
            }

            return array();
        }
    }

    public function getStreetSearch($criteria, $query) {
        $criteria2 = new CDbCriteria;
        $criteria2->condition = "t.PostCode LIKE '%$query%'";
        $criteria2->addCondition("t.FullAddress LIKE  '%$query%'", 'OR');
        $criteria2->order = $criteria->order;
        $criteria2->join = $this->criteria->join;
        $criteria2->addCondition($criteria->condition);
        return $criteria2;
    }

    //run query too for properties without post code like LANd etc
    protected function getPropertiesWithoutGeo($propertyIds) {
        $query = $this->query;
        $criteria2 = new CDbCriteria;
        $criteria2->addCondition($this->criteria->condition, 'AND');
        $criteria2->condition = "char_length(t.PostCode)<1";
        $criteria2->addCondition("t.FullAddress LIKE '%$query%'");
        $criteria2->order = $this->criteria->order;
        $criteria2->join = $this->criteria->join;
        $criteria2->select = 't.lot_id';
        $properties = PropertyAuctionLots::model()->findAll($criteria2);
        return array_unique(array_merge($propertyIds, getInterestedKey($properties, 'lot_id')));
    }

    public function PostCodeSearch($criteria) {
        $d = "(3959 * acos( cos( radians('" . $this->geoPoints['lat'] . "') ) * cos( radians( t.latitude ) ) * 
                            cos( radians( t.longitude ) - radians('" . $this->geoPoints['lng'] . "') ) + 
                                sin( radians('" . $this->geoPoints['lat'] . "') ) * sin( radians( t.latitude ) ) ) )";
        if (!$this->radius && $this->geoPoints['types'][0] == 'postal_code') {//if area only is set default to 0.5miles radius
            $criteria->addCondition("$d <= 0.5");
            $this->radius = 0.5;
            //dump($criteria,true);
        } else {
            $criteria->addCondition("$d <=" . $this->radius);
        }

        return PropertyAuctionLots::model()->findAll($criteria);
    }

    /**
     * REplace criteria param values in criteria condition
     * @return CDBCriteria
     */
    protected function replaceCondition() {
        foreach ($this->criteria->params as $k => $value) {
            $this->criteria->condition = str_replace($k, $value, $this->criteria->condition);
        }
        $this->criteria->params = array();
        return $this->criteria;
    }

}

