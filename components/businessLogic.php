<?php
namespace app\components;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of businessLogic
 *
 * @author Barnabas
 */
class businessLogic {
    const RULE_VIEW_PROPERTY = 'view_property_details';
    const RULE_CAN_UPLOAD_PROPERTY = 'can_upload_property';
    const RULE_CAN_SEND_MESSAGES = 'can_send_messages';
    
    public function __construct() {
        
    }
    
    public static function canViewPropertyDetails($userId = null)
    {
        return true;
    }
    
    public static function canSendMessages($userId = null)
    {
        return true;
    }
    
    public function canUploadProperty($userId = null)
    {
        if(isGuest()) return false;
        if(!identity()->isLandlord()) return false;
        
        return $this->hasPaid();
    }
    
    private function hasPaid($userId = 0)
    {
        //TODO run query to db and check if 
        
        return true;
    }
    
    
}
