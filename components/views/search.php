<?php

/* 
 * Search form
 */
?>
<div class="sidebar-box margin40">
    <h4><?php echo lang('Search');?></h4>
    <form role="form" class="search-widget" action="<?php echo yii\helpers\Url::to(['/search'])?>">
        <input type="text" class="form-control" name="searchterm">
        <i class="fa fa-search"></i>
    </form>
</div><!--sidebar-box-->
