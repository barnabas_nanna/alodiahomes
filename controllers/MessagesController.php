<?php

namespace app\controllers;

use Yii;
use app\models\Messages;
use app\models\MessagesSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Users;
use app\components\Controller;
use app\models\Property;

/**
 * MessagesController implements the CRUD actions for Messages model.
 */
class MessagesController extends Controller {

    public $defaultAction = 'inbox';

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Messages models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MessagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionInbox() {
        $searchModel = new MessagesSearch();
        $dataProvider = Users::getMessages($searchModel);
        
        return $this->render('inbox', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionSent() {
        $searchModel = new MessagesSearch();
        $dataProvider = Users::getSentMessages($searchModel);
        
        return $this->render('sent', [
//                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Messages model.
     * @param string $id
     * @return mixed
     */
    public function actionRead($id) {
        $model = $this->findModel($id);
        if (!in_array(userId(), [$model->getReceiverId(), $model->getSenderId()]) OR $model->isDeleted()) {
            return $this->redirect('inbox', 302);
        }
        return $this->render( $model->isSender() ? 'see' : 'read', [
                    'model' => $model,
        ]);
    }
    
    public function actionNew($propertyid = 0, $userid = 0)
    {
        if(isGuest())
        {
            return $this->redirect(['/site/login']);
        }
        
        if($propertyid)
        {
           return $this->contactLandLord($propertyid);
           
        }elseif($userid)
        {
           return $this->contactUser($userid);
           
        }else
        {
            return $this->redirect(['inbox']);
        }
        
    }
    
    private function contactUser($userid = 0)
    {
        
    }
    
    private function contactLandLord($propertyid = 0)
    {
        $property = Property::findOne($propertyid);
        if(!$property instanceof Property)
        {
            return $this->redirect(['inbox']);
        }
            $modelForm = new Messages();
            $modelForm->setAttribute('message_parent_id', 0);
            $modelForm->setAttribute('from',  userId());
            $modelForm->setAttribute('to', $property->getLandLordId());
            $modelForm->setAttribute('property_id', $property->getPropertyId());
            
            if ($modelForm->load(Yii::$app->request->post())) {
            if ($modelForm->save()) {
                setFlash('success', lang('Message has been sent.'));
                return $this->redirect(['/property/details', 'id' => $property->getPropertyId()]);
            } else {
                setFlash('error', lang('Message has not been sent.'));
            }
        }

            return $this->render('new', [
                        'property' => $property,
                        'modelForm' => $modelForm,
                
            ]);
        
    }

    public function actionReply($id) {

        $model = $this->findModel($id);
        if ($model->getReceiverId() !== userId() OR $model->isDeleted()) {
            return $this->redirect('inbox', 302);
        }

        $modelForm = new Messages();
        $modelForm->setAttribute('message_parent_id', $model->getPrimaryKey());
        $modelForm->setAttribute('from', $model->getSenderId());
        $modelForm->setAttribute('to', $model->getReceiverId());

        if ($modelForm->load(Yii::$app->request->post())) {
            if ($modelForm->save()) {
                setFlash('success', lang('Message has been sent.'));
                return $this->redirect(['inbox', 'id' => $modelForm->getPrimaryKey()]);
            } else {
                setFlash('error', lang('Message has not been sent.'));
            }
        }

        return $this->render('reply', [
                    'model' => $model,
                    'modelForm' => $modelForm
        ]);
    }

    /**
     * Displays a single Messages model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Messages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Messages();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->message_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Messages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->message_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Messages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        if(identity()->isAdmin())
        {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
            
        }else
        {
            $this->findModel($id)->hide();
            return $this->redirect(['inbox']);
        }
        

        
    }

    /**
     * Finds the Messages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Messages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Messages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
