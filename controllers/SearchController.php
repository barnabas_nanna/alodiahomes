<?php
namespace app\controllers;

use yii\filters\AccessControl;
use app\models\Users;
use app\components\Controller;
use Yii;
use yii\data\Pagination;
use app\models\Property;
/**
 * Description of SearchController
 *
 * @author Barnabas
 */
class SearchController extends Controller {
    
    public $defaultAction = 'property';
    
    public function actionProperty($searchterm = '')
    {
        $userid = isGuest() ? 0 : userId();
        $searcnForm = new \app\models\forms\SearchForm();
        $searcnForm->setSearchTerm($searchterm);
        
        if(!empty($searchterm))
        {
        
        $query = $searcnForm->runQuery();
        
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->setPageSize(15);
        $models = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
        $recentProperties = \app\models\RecentProperty::getRecent($userid);
        $savedProperties = \app\models\SavedProperty::getRecent($userid);
        $mostViewProperties = Property::mostViewed(5);
        $view = 'displayResults';
                
        
            return $this->render($view, [
                        'properties' => $models,
                        'pages' => $pages,
                        'recentProperties' => $recentProperties,
                        'savedProperties' => $savedProperties,
                        'mostViewProperties' => $mostViewProperties
            ]);
        }else
        {
            $this->redirect(['/property/display']);
        }
        
    }
    
    public function actionIndex()
    {
        echo 'not developed yet';
    }
}
