<?php

namespace app\controllers;

use yii\filters\AccessControl;
use app\models\Users;
use app\components\Controller;
use Yii;

class AccountController extends Controller
{
    public $defaultAction ='user';
    public function _behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            
        ];
    }
    
    public function _actionIndex()
    {
        
    }
    
    public function _actionUser()
    {
        $this->layout ='/column2';
        $propertyForm = new \app\models\forms\PropertyForm;
        //$propertyForm->scenario = 'addProperty';
        $user = Users::findOne(['email'=>identity()->username]);
        $profile = $user->getProfile()->one();
        
        if ($propertyForm->load(Yii::$app->request->post())) {
            if ($propertyForm->validate() && $propertyForm->addProperty()) {
                // form inputs are valid, do something here
                setFlash('success',lang('Property detials saved. Click approve button.'));
                $this->redirect(['property/new', 'id'=>$propertyForm->propertyId]);
            }else
            {
                setFlash('error','Property not saved. Fix errors and try again.');
            }
        }
        return $this->render('user', compact('user','profile','propertyForm'));
    }
    
    public function actionUser()
    {
        $this->layout ='/column2';
        $propertyForm = new \app\models\forms\PropertyForm;
        //$propertyForm->scenario = 'addProperty';
        $user = Users::findOne(['email'=>identity()->username]);
        $profile = $user->getProfile()->one();
        $savedProperties = \app\models\SavedProperty::getRecent(userId(),5);
        $recentProperties = \app\models\RecentProperty::getRecent(userId(),5);
        $recentMessages = \app\models\Messages::getRecent(userId(),5,true);//recent unread messages
        
        return $this->render('user', compact('user','profile','propertyForm','recentMessages',
                'savedProperties','recentProperties'));
    }
    
    public function actionEdit()
    {
        
        $model = new \app\models\forms\AccountForm();
        $user = Users::findOne(userId());
        $profile = $user->getProfile()->one();
        if(app()->request->isPost)
        {
            $model->setScenario('edit');
            $model->load(Yii::$app->request->post());
            $model->account_type = $user->getStatus();
            if($model->updateAccount($user,$profile))
            {
                setFlash('success', 'Account details have been updated.');
                $this->redirect(['user']);
            }else{
                setFlash('error','Account details could not be updated. Please fix errors and try again.');
            }
            
        }
        
        $model->setAttributes([
            'email'=>$user->getEmail(),
            'firstname'=>$profile->getFirstName(),
            'lastname'=>$profile->getLastName(),
            'title'=>$profile->getTitle()
                ]);
        return $this->render('edit',compact('user','model'));
    }
    
    public function actionId($id)
    {
       
        $this->layout ='/column2';
        $userProperties = $sharedMessages = [];
        $user = Users::findOne(['id'=>$id]);
        $profile = $user->getProfile()->one();
        if($user->isLandLord())
        {
            $userProperties = \app\models\Property::findAll(['user_id'=>$user->getPrimaryKey(), 
                'deleted' => \app\models\Property::PROPERTY_MODE_SHOW]);
        }
                 
        return $this->render('id', compact(
                'user',
                'profile',
                'userProperties',
                'sharedMessages'
                )
                );
    
    }
    
    

}
