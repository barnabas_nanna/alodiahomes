<?php

namespace app\controllers;

class AdminController extends \app\components\Controller
{
    public $defaultAction = 'dashboard';
    public function actionDashboard()
    {
        if(isGuest() OR !identity()->isAdmin()) return $this->goHome ();
        return $this->render('dashboard');
    }

}
