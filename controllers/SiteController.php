<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\forms\AccountForm;
use app\models\Faq;
use yii\data\Pagination;

class SiteController extends Controller
{
    public $defaultAction = 'home';
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionHome()
    {
        
        $this->layout = '/column1';
        return $this->render('home');
    }

    public function actionLogin()
    {
        $this->layout = '/column1';
        if (!isGuest()) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()))
        {
            if($model->login()) {
               setFlash('success', 'Welcome back.');
               return $this->redirect(['/account']);
           } else {
               setFlash('error', 'Please insert the right details.');
           }
        
        }
        
            return $this->render('login', [
                'model' => $model,
            ]);
        
    }
    
    public function actionRegister()
    {
        if(!isGuest())
        {
            return $this->getHome();
        }
        $model = new AccountForm();
        $model->scenario = 'register';
        $this->layout = '/column1';

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->register()) {
                // form inputs are valid, do something here
                 setFlash('success','Registration was successful.');
                 $this->redirect(['login']);
            }else
            {
                setFlash('error','Registration was not successful. Do try again.');
            }
        }

        return $this->render('register', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        $this->layout = '/column1';
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            setFlash('success', 'Contact form submitted successfully. We will be in touch.');
            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        $this->layout = '/column1';
        return $this->render('about');
    }
    
    public function actionFaq()
    {
        return $this->FAQ();
    }
    
    public function actionValidate($token,$email)
    {
        if(!isGuest())
        {
           return $this->goHome();
        }
        
        $model = new AccountForm();
        if($model->validateToken($email, $token))
        {
            setFlash('success',lang('Your password has been updated successfully'));
            $this->redirect(['login']);
        }else{
            setFlash('error',lang('Reset link is no longer valid. Request another.'));
            $this->redirect(['resetpassword']);
        }
        
        
    }


    public function actionResetpassword()
    {
        $this->layout = '/column1';
        if(!isGuest())
        {
           return $this->goHome ();
        }
        
        $model = new AccountForm();
        $model->scenario = 'resetpassword';

        if ($model->load(Yii::$app->request->post())) {
            
            if ($model->validate() && $model->saveNewPassword()) {
                //form inputs are valid, do something here
                 //$this->redirect(['login']);
            }
            
            setFlash('success','A reset link has been sent to your inbox if that account exists.');
            
        }

        return $this->render('resetpassword', [
            'model' => $model,
        ]);
    }
    
    protected function FAQ()
    {
       $query = Faq::find(['deleted' => Faq::FAQ_SHOW]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->setPageSize(10);
        $models = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
        
        return $this->render('faq', [
                    'questions' => $models,
                    'pages' => $pages,
                    
        ]);
    }
}
