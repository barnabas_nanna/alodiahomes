<?php

namespace app\controllers;

use Yii;
use app\models\Property;
use yii\data\ActiveDataProvider;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\forms\PropertyForm;
use app\models\Users;
use app\models\SavedProperty;
use yii\data\Pagination;

/**
 * PropertyController implements the CRUD actions for Property model.
 */
class PropertyController extends Controller {

    public $adminActions = 'stats';

    public function _behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Property models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Property::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Property model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Property model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Property();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->property_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Property model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->property_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Property model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->goDeleteMode();

        return $this->redirect(['index']);
    }

    public function actionDeleteProperty($id)
    {
        $p = $this->findModel($id);
        if ($p instanceof Property && ($p->isOwner() OR isAdmin())) {
            setFlash('success', 'Property has been removed.');
            $p->goDeleteMode();
        }
        $this->goBack();
    }

    /**
     * Finds the Property model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Property the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Property::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDisplay()
    {
        return $this->findProperties('display', 'app\models\Property', ['deleted' => Property::PROPERTY_MODE_SHOW]);
    }

    public function actionRecent()
    {
        if(isGuest())
        {
            $query = Property::find()->where(['property_id'=>\app\models\RecentProperty::getViewedProperties()]);
            return $this->findProperties('recent', 'app\models\RecentProperty',[],$query);
        }else{
            return $this->findProperties('recent', 'app\models\RecentProperty');
        }
        
    }

    public function actionSaved()
    {
        return $this->findProperties('saved', 'app\models\SavedProperty');
    }

    private function findProperties($view, $model, $condition = [], $query=null)
    {
        $userid = 0;
        if(!is_null($query))
        {
            
        }
        elseif ($view !== 'display') {
            $userid = userId();
            $query = $model::find()->where(['user_id' => $userid] + $condition);
        } else {//when showing all properties
            $query = $model::find()->where($condition);
        }
        
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->setPageSize(15);
        $models = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
        $recentProperties = \app\models\RecentProperty::getRecent($userid);
        $savedProperties = \app\models\SavedProperty::getRecent($userid);
        $mostViewProperties = Property::mostViewed(5);

        return $this->render($view, [
                    'properties' => $models,
                    'pages' => $pages,
                    'recentProperties' => $recentProperties,
                    'savedProperties' => $savedProperties,
                    'mostViewProperties' => $mostViewProperties
        ]);
    }

    public function actionProperties()
    {

        return $this->findProperties('properties', 'app\models\Property', ['deleted' => '!=' . Property::PROPERTY_MODE_DELETED]);
    }

    public function actionAdd($id = null)
    {
        if (identity()->student) {
            $this->goHome();
        }
        $propertyForm = new \app\models\forms\PropertyForm;
        $property = null;
        if ($id) {
            $property = $this->findModel($id);
            if (!$property->isOwner()) {
                setFlash('error', 'You can view that property.');
                $this->redirect(['add']);
            } else {
                $propertyForm->attributes = $property->attributes;
            }
        }

        $user = Users::findOne(['email' => Yii::$app->user->identity->username]);
        $profile = $user->getProfile()->one();

        if ($propertyForm->load(Yii::$app->request->post())) {
            if ($propertyForm->validate() && $propertyForm->addNewProperty($property)) {
                // form inputs are valid, do something here
                setFlash('success', 'Property details saved.');
                $this->redirect(['new', 'id' => $propertyForm->propertyId]);
            } else {
                setFlash('error', 'Property details not saved.');
            }
        }

        return $this->render('add', compact('user', 'profile', 'propertyForm', 'property'));
    }

    public function actionNew($id)
    {
        $model = new PropertyForm();
        $property = $this->findModel($id);
        $propertyImages = $property->getImages();
        if (!$property->isOwner()) {//ensure its owner
            $this->goHome();
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->approvePropertyAd($property)) {
                // form inputs are valid, do something here
                setFlash('success', 'Property listing is now live.');
            } else {
                setFlash('error', 'Property listing is not live.');
            }
        }

        return $this->render('new', [
                    'model' => $model,
                    'property' => $property,
                    'propertyImages' => $propertyImages
        ]);
    }

    public function actionDetails($id)
    {
        $model = new PropertyForm();
        $property = $this->findModel($id);
        if (!$property->isLive()) {
            $this->goHome();
        }
        $propertyImages = $property->getImages();
        \app\models\RecentProperty::addToHistory($property);

        return $this->render('details', [
                    'model' => $model,
                    'property' => $property,
                    'propertyImages' => $propertyImages
        ]);
    }

    public function actionContactLandlord()
    {
        $propertyId = (int) Yii::$app->request->post('propertyId');

        $result = array('error' => true);

        if (!isGuest() && Yii::$app->request->isPost && $propertyId) {
            if (Property::emailLandlord($propertyId, userId())) {
                $result = array('error' => false, 'message' => 'Landlord has been contacted.');
            }
        } elseif (isGuest()) {
            $result['message'] = 'Need to sign in first';
        }

        echo json_encode($result);
    }

    public function actionSaveproperty()
    {
        $propertyId = (int) Yii::$app->request->post('propertyId');

        $result = array('error' => true);

        if (!isGuest() && Yii::$app->request->isPost && $propertyId) {
            if (Property::addToSavedProperty($propertyId, userId())) {
                $result = array('error' => false, 'message' => 'Property saved.');
            }
        } elseif (isGuest()) {
            $result['message'] = 'Need to sign in first';
        }

        echo json_encode($result);
    }
    
    /**
     * Lists all Property models.
     * @return mixed
     */
    public function actionStats()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Property::find()->where(['deleted'=>  Property::PROPERTY_MODE_SHOW]),
            'sort'=> [
                'defaultOrder' => ['views' => SORT_DESC]
            ]
        ]);

        return $this->render('stats', [
                    'dataProvider' => $dataProvider,
        ]);
    }

}
