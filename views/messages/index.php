<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MessagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Messages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="messages-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class='divide20'></div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <? //echo Html::a(Yii::t('app', 'Create Messages'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //'message_id',
            [
              'header'=>'From',
              'attribute' => 'sender.fullname',
            ],
            'receiver.fullname',
            'title',
            'created_date:date:Date',//'attribute:format:Label',
            'deleted:deleted',
            // 'modified_date',
            // 'content:ntext',
            // 'read',
            // 'deleted',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
