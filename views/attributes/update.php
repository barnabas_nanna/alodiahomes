<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Attributes */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Attributes',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Attributes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->attribute_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="attributes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
