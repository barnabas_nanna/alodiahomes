<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Blogs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="divide20"></div>
    <p>
        <?= Html::a(Yii::t('app', 'Create Blog'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'blog_id',
            'title',
            'created_date:date',
            
           // 'post:ntext',
            'writer.fullname',
             'deleted:deleted',
            'categoryname',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
