<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\cms */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cms-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cms_admin_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cms_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cms_content')->widget(\yii\redactor\widgets\Redactor::className()) ?>

    <?= $form->field($model, 'deleted')->dropDownList(['No','Hide','Yes']) ?>
    <hr/>
    <?= $form->field($model, 'cms_description')->textarea(['rows' => 6]) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
