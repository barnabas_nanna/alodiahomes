<?php

namespace app\models;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $status;
    public $landlord = false;
    public $student = false;
    public $admin = false;
    public $viewedProperties = [];


    /**
     * Create a IdentityInterface from a users object
     * @param \app\models\Users $users
     * @return \app\models\User
     */
    public static function createUser(Users $users)
    {
        $user = new User();
        $user->id = $users->getPrimaryKey();
        $user->status = $users->status;
        $user->landlord = $users->isLandLord();
        $user->student  = $users->isStudent();
        $user->admin    = $users->isAdmin();
        $user->username = $users->email;
        $user->password = $users->password;
        $user->authKey = 'keke'.$user->id.'key';
        $user->accessToken = 'accesskeke'.$user->id.'token';
        
        return $user;
    }
        private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $users =  Users::findOne($id);
        if(!is_null($users))
        {
            return self::createUser($users);
        }
        
        return null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {        
       $users =  Users::findOne(['email'=> $username]);
       if(!is_null($users))
       {
           return self::createUser($users);
       }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
    
    public function isAdmin()
    {
        return $this->admin;
    }
    
    public function isLandlord()
    {
        return $this->landlord;
    }
    
    public function isStudent()
    {
        return $this->student;
    }
    
    public function addToViewedProperty(Property $property)
    {
        $p = app()->session->get('viewedProperties',[]);
        array_push($p,$property->getPropertyId());
        return app()->session->set('viewedProperties',$p);

    }
    
    public function getViewedProperties()
    {
        return app()->session->get('viewedProperties',[]);
    }
}
