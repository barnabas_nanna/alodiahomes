<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FilesUsage]].
 *
 * @see FilesUsage
 */
class FilesUsageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return FilesUsage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FilesUsage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}