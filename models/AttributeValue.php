<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "attribute_value".
 *
 * @property string $value_id
 * @property string $attribute_id
 * @property string $element_id
 * @property integer $deleted
 * @property string $value
 * @property integer $section
 *
 * @property Attributes $attribute
 */
class AttributeValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attribute_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_id', 'element_id', 'deleted', 'section'], 'integer'],
            [['element_id'], 'required'],
            [['value'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'value_id' => Yii::t('app', 'Value ID'),
            'attribute_id' => Yii::t('app', 'Attribute ID'),
            'element_id' => Yii::t('app', 'Element ID'),
            'deleted' => Yii::t('app', 'Deleted'),
            'value' => Yii::t('app', 'Value'),
            'section' => Yii::t('app', 'Section'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttribute()
    {
        return $this->hasOne(Attributes::className(), ['attribute_id' => 'attribute_id']);
    }

    /**
     * @inheritdoc
     * @return AttributeValueQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AttributeValueQuery(get_called_class());
    }
}
