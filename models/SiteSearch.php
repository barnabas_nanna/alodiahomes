<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_search".
 *
 * @property integer $search_id
 * @property string $search_term
 * @property string $user_id
 * @property string $created_date
 */
class SiteSearch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_search';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search_id', 'search_term', 'user_id', 'created_date'], 'required'],
            [['search_id', 'user_id', 'created_date'], 'integer'],
            [['search_term'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'search_id' => Yii::t('app', 'Search ID'),
            'search_term' => Yii::t('app', 'Search Term'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_date' => Yii::t('app', 'Created Date'),
        ];
    }

    /**
     * @inheritdoc
     * @return SiteSearchQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SiteSearchQuery(get_called_class());
    }
}
