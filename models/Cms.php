<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cms".
 *
 * @property string $id
 * @property string $cms_admin_id
 * @property string $cms_title
 * @property string $cms_content
 * @property integer $user_id
 * @property integer $deleted
 * @property string $created_date
 * @property string $modified_date
 * @property string $cms_description
 */
class Cms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cms_admin_id', 'cms_title', 'cms_content', 'user_id', 'deleted', 'created_date'], 'required'],
            [['cms_content','cms_description'], 'string'],
            [['user_id', 'deleted', 'created_date'], 'integer'],
            [['modified_date'], 'safe'],
            [['cms_admin_id'], 'string', 'max' => 50],
            [['cms_title'], 'string', 'max' => 250],
            [['cms_admin_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cms_description' => lang('Cms Description'),
            'id' => Yii::t('app', 'ID'),
            'cms_admin_id' => Yii::t('app', 'Cms Admin ID'),
            'cms_title' => Yii::t('app', 'Cms Title'),
            'cms_content' => Yii::t('app', 'Cms Content'),
            'user_id' => Yii::t('app', 'User ID'),
            'deleted' => Yii::t('app', 'Deleted'),
            'created_date' => Yii::t('app', 'Created Date'),
            'modified_date' => Yii::t('app', 'Modified Date'),
        ];
    }
    
    public function beforeValidate()
    {
        $this->user_id = userId();
        $this->created_date = time();
        return parent::beforeValidate();
    }
    
     public static function getBlock($block_id = '')
    {
        $block = self::findOne(['cms_admin_id' => $block_id,'deleted'=>0]);
        if(!$block instanceof Cms)
        {
            $block = new \app\components\CmsNull();
            $block->cms_title = '';
            $block->cms_content = '';
        }
        
        return $block;
    }
    
    public function getTitle()
    {
        return $this->cms_title;
    }
    
    public function getContent()
    {
        return $this->cms_content;
    }
    
    public function getDescription($textOnly = false)
    {
        if($textOnly){
            return strip_tags($this->cms_description);
        }
        return $this->cms_description;
    }
}
