<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "property".
 *
 * @property string $property_id
 * @property string $house_no
 * @property string $address_line_1
 * @property string $address_line_2
 * @property string $address_line_3
 * @property string $post_code
 * @property string $coordinates
 * @property string $description 
 * @property string $key_features
 * @property integer $user_id
 * @property integer $let_type
 * @property integer $price_frequency
 * @property integer $bedrooms
 * @property double $price
 * @property int $views
 * @property int $deleted
 */
class Property extends \yii\db\ActiveRecord {

    const PRICE_FREQUENCY_WEEKLY = 1;
    const PRICE_FREQUENCY_MONTHLY = 2;
    const PROPERTY_MODE_SHOW = 0;
    const PROPERTY_MODE_EDIT = 1;
    const PROPERTY_MODE_DELETED = 2;
    const LET_TYPE_ROOM = 1;
    const LET_TYPE_HOUSE = 2;
    const LET_TYPE_FLAT = 3;

    protected $link = null;

    CONST SEPARATOR = '|';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['house_no', 'contact_email', 'address_line_1', 'address_line_2', 'address_line_3', 'availability_date',
            'minimum_tenancy', 'post_code', 'description'], 'required'],
            [['key_features', 'house_no', 'address_line_1', 'address_line_2', 'address_line_3', 'post_code',
            'coordinates', 'description', 'user_id', 'let_type', 'price', 'discount_price',
            'availability_date', 'closing_date', 'contact_tel',
            'contact_email', 'minimum_tenancy', 'pets_preference'
            , 'smoking_preference'], 'safe'],
            [['house_no', 'address_line_1', 'address_line_2', 'address_line_3', 'post_code',
            'description', 'user_id', 'let_type', 'price'], 'required'],
            [['house_no', 'user_id', 'let_type', 'price_frequency', 'bedrooms', 'views'], 'integer'],
            [['description', 'pets_preference', 'minimum_tenancy'], 'string'],
            [['price'], 'number'],
            [['post_code'], 'postcodeCheck'],
            [['availability_date'], 'date', 'format' => 'yyyy-M-d'],
            [['address_line_1', 'address_line_2', 'address_line_3', 'post_code', 'coordinates'], 'string', 'max' => 250]
        ];
    }

    public function postcodeCheck($attribute = '', $params = [])
    {
        $postcode_regex = '/(^[A-Z]{1,2}[0-9][0-9A-Z]?\s?[0-9][A-Z]{2}$)/i';
        if (preg_match($postcode_regex, $attribute)) {
            $this->addError('post_code', lang('Postcode is not valid.'));
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'property_id' => Yii::t('app', 'Property ID'),
            'house_no' => Yii::t('app', 'House No'),
            'address_line_1' => Yii::t('app', 'Address Line 1'),
            'address_line_2' => Yii::t('app', 'Address Line 2'),
            'address_line_3' => Yii::t('app', 'Address Line 3'),
            'post_code' => Yii::t('app', 'Post Code'),
            'coordinates' => Yii::t('app', 'Coordinates'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User ID'),
            'let_type' => Yii::t('app', 'Let Type'),
            'price_frequency' => Yii::t('app', 'Price Frequency'),
            'bedrooms' => Yii::t('app', 'Bedrooms'),
            'price' => Yii::t('app', 'Rental Price'),
        ];
    }

    /**
     * @inheritdoc
     * @return PropertyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PropertyQuery(get_called_class());
    }

    public static function getPriceFrequency()
    {
        return [
            self::PRICE_FREQUENCY_WEEKLY => 'Weekly',
            self::PRICE_FREQUENCY_MONTHLY => 'Monthly'
        ];
    }

    /**
     * 
     * @return int
     */
    public function getPriceFreqValue()
    {
        return $this->price_frequency;
    }

    public static function getLetTypes()
    {
        return [
            self::LET_TYPE_ROOM => 'Room',
            self::LET_TYPE_HOUSE => 'House',
            self::LET_TYPE_FLAT => 'Flat'
        ];
    }

    public function getLetTypeName()
    {
        $t = self::getLetTypes();

        if (array_key_exists($this->let_type, $t)) {
            return $t[$this->let_type];
        }

        return '';
    }

    public function getPriceFrequencyName($lowercase = false)
    {
        $t = self::getPriceFrequency();

        if (array_key_exists($this->price_frequency, $t)) {
            return (false === $lowercase) ? $t[$this->price_frequency] : strtolower($t[$this->price_frequency]);
        }

        return '';
    }

    public function getPrice()
    {
        $price = $this->discount_price > 0 ? $this->discount_price : $this->price;
        return number_format($price);
    }

    public function getImages()
    {
        $sql = "SELECT f.* FROM files f INNER JOIN files_usage fu on fu.files_id=f.file_id"
                . " WHERE section_id={$this->getPrimaryKey()} AND fu.section='property'";

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();

        return $result;
    }

    public function getMainImage()
    {
        $sql = "SELECT f.* FROM files f INNER JOIN files_usage fu on fu.files_id=f.file_id"
                . " WHERE section_id={$this->getPrimaryKey()} AND fu.section='property' ORDER BY file_id ASC LIMIT 1";

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $result = $command->queryOne();

        $src = empty($result['location']) ? self::getDefaultImage() : self::getImageSrc($result['location']);


        return $src;
    }

    public static function getDefaultImage()
    {
        return 'http://placehold.it/255x144?text=Image';
    }

    /**
     * Get image src
     * @param string $location
     * @return string
     */
    public static function getImageSrc($location)
    {
        if (empty($location)) {
            return self::getDefaultImage();
        }
        $position = strpos($location, 'images/');
        $src = substr_replace($location, 'http://' . $_SERVER['SERVER_NAME'], 0, $position - 1);
        return str_replace("\\", '/', $src);
    }

    public function getDisplayAddress($separator = ', ')
    {
        $address = $this->house_no . $separator . $this->address_line_1 . $separator . $this->address_line_2 .
                $separator . $this->address_line_3 . $separator . $this->post_code;
        return $address;
    }

    public function getDisplayPrice()
    {
        $price = $this->getPrice();
        if ($this->getPriceFreqValue() === self::PRICE_FREQUENCY_WEEKLY) {
            $freq = ' p/w';
        } else {
            $freq = ' p/m';
        }

        return $this->getDisplayCurrency() . $price . $freq;
    }

    public function getDisplayCurrency()
    {
        return '&pound';
    }

    public function getPropertyId()
    {
        return $this->getPrimaryKey();
    }

    public static function addToSavedProperty($propertyId = 0, $userId = null, $sendNotificatinEmail = false)
    {
        $property = Property::findOne((int) $propertyId);

        if ($property instanceof Property && $property->isActive()) {
            $userId = (int) $userId;
            $savedProperty = new SavedProperty();
            $savedProperty->user_id = $userId;
            $savedProperty->property_id = $property->getPropertyId();
            $savedProperty->created_date = time();
            $savedProperty->save(false);
            $propertyId = $property->getPropertyId();
            $saveId = $savedProperty->getPrimaryKey();

            if ($saveId) {
                //delete any other occurence of record
                SavedProperty::deleteUserProperty($userId, $propertyId, $saveId);
                if ($sendNotificatinEmail) {
                    //TODO send email
                }
                return true;
            }

            return false;
        }
    }

    public function isActive()
    {
        return $this->isLive();
    }

    public function emailLandlord($userId = 0, $propertyId = 0, $sendNotificationEmail = true)
    {
        
    }

    /**
     * Return landlord user id
     * @return int
     */
    public function getLandLordId()
    {
        return $this->user_id;
    }

    public function getLandLord()
    {
        return Users::findOne($this->user_id);
    }

    public static function getProperty($propertyId = 0)
    {

        $property = self::findOne($propertyId);

        return $property;
    }

    public function getPropertyDetailsLink()
    {
        $url = $this->getPropertyLink();
        return \yii\helpers\Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                    'title' => Yii::t('yii', 'View'),
        ]);
    }

    public function getPropertyLink()
    {
        if (is_null($this->link)) {
            $this->link = \yii\helpers\Url::toRoute(['property/details', 'id' => $this->getPropertyId()]);
        }

        return $this->link;
    }

    public static function mostViewed($limit = 5)
    {
        return static::find()->where(['deleted' => 0])->orderBy('views DESC')->limit($limit)->all();
    }

    public function getKeyFeatures()
    {
        return explode(self::SEPARATOR, $this->key_features);
    }

    public function goLiveMode()
    {
        $this->deleted = self::PROPERTY_MODE_SHOW;
        return $this->save(false);
    }

    public function goEditMode()
    {
        $this->deleted = self::PROPERTY_MODE_EDIT;
        return $this->save(false);
    }

    public function goDeleteMode()
    {
        $this->deleted = self::PROPERTY_MODE_DELETED;
        return $this->save(false);
    }

    /**
     * Is the property in live mode
     * @return type
     */
    public function isLive()
    {
        return $this->deleted == self::PROPERTY_MODE_SHOW;
    }

    public function isOwner($userId = null)
    {
        if (!isGuest() && is_null($userId)) {
            $userId = userId();
        }

        return empty($userId) ? false : ($this->user_id === $userId ) ? true : false;
    }

    public function toDisplayString()
    {
        $pieces = [];
        $pieces[] = '<div class="product-col">';
        if (!$this->isActive()):
            $pieces[] = '<span class="sale-label">' . lang("No Longer Available") . '</span>';
        endif;

        $pieces[] = '<a href="' . ( $this->isLive() ? $this->getPropertyLink() : 'javascript:;') . '">
                <img src ="' . $this->getMainImage() . '" alt="" class="img-responsive">
            </a>';
        $pieces[] = '<div class="product-desc-sh">
                <span class="price">' . $this->getDisplayPrice() . '</span>
                <a href="' . ( $this->isLive() ? $this->getPropertyLink() : 'javascript:;') . '" class="btn btn-default">View Property</a>
            </div>                     
        </div>';

        return implode(PHP_EOL, $pieces);
    }

}
