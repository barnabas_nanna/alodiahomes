<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SiteSearch]].
 *
 * @see SiteSearch
 */
class SiteSearchQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SiteSearch[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SiteSearch|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}