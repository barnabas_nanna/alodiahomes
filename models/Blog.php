<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blog".
 *
 * @property integer $blog_id
 * @property integer $created_date
 * @property string $title
 * @property string $post
 * @property integer $user_id
 * @property integer $deleted
 * @property integer $category_id
 */
class Blog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_date', 'user_id', 'deleted', 'category_id'], 'integer'],
            [['title', 'post', 'user_id', 'deleted', 'category_id'], 'required'],
            [['post'], 'string'],
            [['title'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'blog_id' => Yii::t('app', 'Blog ID'),
            'created_date' => Yii::t('app', 'Created Date'),
            'title' => Yii::t('app', 'Title'),
            'post' => Yii::t('app', 'Post'),
            'user_id' => Yii::t('app', 'User ID'),
            'deleted' => Yii::t('app', 'Deleted'),
            'category_id' => Yii::t('app', 'Category ID'),
        ];
    }
    
    public function beforeValidate()
    {
        $this->user_id = userId();
        $this->created_date = time();
        return parent::beforeValidate();
    }
    
    public function getCategoryname()
    {
        return Category::findOne(['category_id'=>$this->category_id])->title;;
    }
    
    public function getWriter()
    {
        return Profile::findOne(['user_id'=>$this->user_id]);
    }
    
    public function getWritername()
    {
        return $this->getWriter()->getFullName();
    }
    
    public function toDisplayString($single = false)
    {
        $str = [];
        $str[] = '<div class="blog-post">';
                        $str[] = '<a href="'.$this->getUrl(false).'">';
                            $str[] = '<div class="item-img-wrap">';
                                $str[] = '<img alt="workimg" class="img-responsive" src="/assan/img/showcase-2.jpg">';
                                $str[]='<div class="item-img-overlay">';
                                   $str[] = '<span></span>';
                                 $str[] = '</div>';
                             $str[] = '</div>';                       
                         $str[] = '</a>';
                         $str[] = '<ul class="list-inline post-detail">';
                             $str[] = '<li>by <a href="#">'.$this->getWritername().'</a></li>';
                             $str[] = '<li><i class="fa fa-calendar"></i>'.app()->formatter->asDate($this->created_date).'</li>';
                             $str[]= '<li><i class="fa fa-tag"></i> <a href="#">'.$this->getCategoryname().'</a></li>';
                        $str[]= '</ul>';
                         $str[] = '<h2><a href="'.$this->getUrl(false).'">'.$this->getTitle().'</a></h2>';
                         $str[] = '<p>';
                         $str[]= ($single) ? $this->post : substr($this->post, 0, 30);
                         $str[]= '</p>';
                         if(!$single)
                         {
                            $str[]= '<p><a class="btn btn-theme-dark" href="'.$this->getUrl(false).'">Read More...</a></p>';
                         }
                         $str[] = '</div>';
                     
                     return implode(PHP_EOL, $str);
    }
    
    
    public function getUrl($link = true)
    {
        $url = \yii\helpers\Url::to(['post','id'=>$this->getPrimaryKey()]);
        if(!$link)
        {
            return $url;
        }
        return \yii\helpers\Html::a($this->getTitle(), $url);
    }
    
    public function getTitle()
    {
        return $this->title;
    }
    
    public function isDeleted()
    {
        return $this->deleted === DELETED;
    }
}
