<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "albums".
 *
 * @property string $album_id
 * @property string $user_id
 * @property string $name
 * @property string $created_at
 * @property string $modified_date
 * @property string $notes
 * @property integer $access_type
 */
class Albums extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'albums';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'created_at', 'notes'], 'required'],
            [['user_id', 'created_at', 'access_type'], 'integer'],
            [['modified_date'], 'safe'],
            [['notes'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'album_id' => Yii::t('app', 'Album ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'name' => Yii::t('app', 'Name'),
            'created_at' => Yii::t('app', 'Created At'),
            'modified_date' => Yii::t('app', 'Modified Date'),
            'notes' => Yii::t('app', 'Notes'),
            'access_type' => Yii::t('app', 'Access Type'),
        ];
    }

    /**
     * @inheritdoc
     * @return AlbumsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AlbumsQuery(get_called_class());
    }
}
