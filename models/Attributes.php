<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "attributes".
 *
 * @property string $attribute_id
 * @property integer $sector
 * @property string $description
 * @property string $name
 * @property string $admin_name
 *
 * @property AttributeValue[] $attributeValues
 */
class Attributes extends \yii\db\ActiveRecord
{
    
    
    const SECTOR_STUDENT = 1;
    const SECTOR_LANDLORD = 2;
    const SECTOR_PROPERTY = 3;
    const SECTOR_STUDENT_AND_LANDLORD = 4;
    
    /**
     * All sectors
     * @return array
     */
    public function getSectorDescriptions()
    {
        return [
            self::SECTOR_STUDENT => 'Student',
            self::SECTOR_LANDLORD => 'Landlord',
            self::SECTOR_STUDENT_AND_LANDLORD => 'Student and Landlord',
            self::SECTOR_PROPERTY => 'Property'
        ];
    }
    
    /**
     * Return the sector name
     * @return string
     */
    public function getSectorName()
    {
        $names = $this->getSectorDescriptions();
        if(array_key_exists($this->sector, $names))
        {
            return $names[$this->sector];
        }
        
        return '';
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attributes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sector'], 'integer'],
            [['admin_name'], 'required'],
            [['description', 'name', 'admin_name'], 'string', 'max' => 250],
            [['admin_name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'attribute_id' => Yii::t('app', 'Attribute ID'),
            'sector' => Yii::t('app', 'Sector'),
            'description' => Yii::t('app', 'Description'),
            'name' => Yii::t('app', 'Name'),
            'admin_name' => Yii::t('app', 'Admin Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeValues()
    {
        return $this->hasMany(AttributeValue::className(), ['attribute_id' => 'attribute_id']);
    }

    /**
     * @inheritdoc
     * @return AttributesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AttributesQuery(get_called_class());
    }
}
