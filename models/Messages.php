<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "messages".
 *
 * @property string $message_id
 * @property string $from
 * @property string $to
 * @property string $message_parent_id
 * @property string $created_date
 * @property string $modified_date
 * @property string $content
 * @property string $read
 * @property string $deleted
 * @property int $property_id
 *
 * @property Users $from
 * @property Users $to
 */
class Messages extends \yii\db\ActiveRecord
{
    
    const DISPLAY = 0;
    const DELETED = 2;
    const SUSPENDED = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'required'],
            [['from', 'to', 'message_parent_id', 'created_date', 'read', 'deleted','hide_in_inbox','hide_in_sent','property_id'], 'integer'],
            [['modified_date','content','title'], 'safe'],
            [['content','title'], 'string']
        ];
    }
    
    


    public function beforeValidate() {
        if($this->getIsNewRecord())
        {
            $this->read = 0;
            $this->deleted = 0;
            $this->created_date = time();
            
            
        }
        return parent::beforeValidate();
    }
    
    public function hide()
    {
        if(userId() === $this->getSenderId())
        {
            //hide in sent folder
            $this->hide_in_sent = 1;
            $this->update(false,['hide_in_sent']);
        }
        
        if(userId() === $this->getReceiverId())
        {
            //hide in inbox
            $this->hide_in_inbox = 1;
            $this->update(false,['hide_in_inbox']);
        }
        
        
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'message_id' => Yii::t('app', 'Message ID'),
            'from' => Yii::t('app', 'From'),
            'to' => Yii::t('app', 'To'),
            'message_parent_id' => Yii::t('app', 'Message Parent ID'),
            'created_date' => Yii::t('app', 'Created Date'),
            'modified_date' => Yii::t('app', 'Modified Date'),
            'content' => Yii::t('app', 'Message'),
            'read' => Yii::t('app', 'Read'),
            'deleted' => Yii::t('app', 'Deleted'),
        ];
    }
    
    /**
     * Profile record of the one who sent the message
     * @return type
     */
    public function getSender()
    {
        return $this->getFrom()->one();
    }
    
    /**
     * Profile records of the one message was sent to
     * @return Active Record of Profile
     */
    public function getReceiver()
    {
        return $this->getTo()->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrom()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTo()
    {
        
        return $this->hasOne(Profile::className(), ['user_id' => 'to']);
                
    }

    /**
     * @inheritdoc
     * @return MessagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MessagesQuery(get_called_class());
    }
    
    /**
     * user_id of message receiver
     * @return int
     */
    public function getReceiverId()
    {
        return $this->to;
    }
    
    /**
     * user_id of message sender
     * @return int
     */
    public function getSenderId()
    {
        return $this->from;
    }
    
    /**
     * Is user is sender of message.
     * If user not set then logged in user is used
     * @params int|nul $userId
     * @return boolean true if true
     */
    public function isSender($userId = null)
    {
        if(is_null($userId))
        {
            $userId = userId();
        }
            
        return $userId === $this->getSenderId();
    }


    /**
     * Is message deleted
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->deleted === self::DELETED;
    }
    
    
    public function getPropertyId()
    {
        return $this->property_id;
    }
    
    public static function getRecent($userid = 0, $limit = 5, $unread = false)
    {
        $filter = ($unread) ? ['deleted' => 0, 'to'=>$userid,'hide_in_inbox'=>0, 'read'=>0] : 
             ['deleted' => 0, 'to'=>$userid,'hide_in_inbox'=>0];
         return self::find()
            ->where($filter)
            ->limit($limit)
            ->all();
    }
    
    public function getInboxLink()
    {
        $url = \yii\helpers\Url::toRoute(['/messages/read','id'=>$this->getPrimaryKey()]);
        return \yii\helpers\Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                'title' => Yii::t('yii', 'View'),
        ]);
    }
    
    public function getLink()
    {
        return $url = \yii\helpers\Url::toRoute(['/messages/read','id'=>$this->getPrimaryKey()]);
        
    }
}
