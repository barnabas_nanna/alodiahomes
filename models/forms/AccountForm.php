<?php
namespace app\models\forms;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountForm
 *
 * @author BarnabasU
 */
use app\models\Users;
use app\models\Profile;

class AccountForm extends \yii\base\Model{
    //put your code here
    public $firstname;
    public $lastname;
    public $title;
    public $email;
    public $password;
    public $new_password;
    public $confirm_password;
    public $account_type;
    public $id;//user id

    const ACCOUNT_TYPE_STUDENT = 1;
    const ACCOUNT_TYPE_LANDLORD = 2;
    const ACCOUNT_TYPE_ADMIN = 10;
    
    public static function getPublicAccountTypes()
    {
        return [
            self::ACCOUNT_TYPE_STUDENT => 'Student',
            self::ACCOUNT_TYPE_LANDLORD => 'Landlord',
        ];
    }
    
    public static function getAllAccountTypes()
    {
        return [
            self::ACCOUNT_TYPE_STUDENT => 'Student',
            self::ACCOUNT_TYPE_LANDLORD => 'Landlord',
            self::ACCOUNT_TYPE_ADMIN => 'Admin'
        ];
    }


    public function rules() {
        return [
            [['firstname','lastname','email','password','confirm_password','account_type'],'required','on'=>'register'],
            [['email','new_password','confirm_password'],'required','on'=>'resetpassword'],
            [['firstname','lastname','email','account_type'],'required','on'=>'edit'],
            [['firstname','lastname','email','password','confirm_password','account_type','title'],'safe'],
            [['email'],'email'],
            [['account_type'], 'integer'],
            [['account_type'], 'validAccountType'],
            [['email'],'uniqueEmail','on'=>'register'],
            [['email'],'uniqueEmail2','on'=>'edit'],
            ['confirm_password', 'compare', 'compareAttribute' => 'password','on'=>'register'],
            ['confirm_password', 'compare', 'compareAttribute' => 'new_password','on'=>'resetpassword']

        ];
    }
    
    public function validAccountType($attribute = '', $params = [])
    {
        if(isGuest())
        {
            return in_array($this->account_type, $this->getPublicAccountTypes());
        }
        
        if(identity()->isAdmin())
        {
            return in_array($this->account_type, $this->getAllAccountTypes());
        }
        
        return false;
    }




    public function uniqueEmail($attribute = '', $params = [])
    {
        if(!is_null(Users::findOne(['email' => $this->email])))
        {
            $this->addError('email','Email ('.$this->email.') has already been taken');
            return false;
        }
        else
        {
            return true;
        }
    }
    
    


    /**
     * Check is anyone else is using the email address
     * @param string $attribute
     * @param array $params
     * @return boolean
     */
    public function uniqueEmail2($attribute = '', $params = [])
    {
        $r = Users::find(['email' => $this->email])
                ->where(['id' => '!='.userId()])
                ->select(['email','id'])->one();
        if(!empty($r))
        {
           $this->addError('email','Email ('.$this->email.') has already been taken');
            return false;
        }
        else
        {
            return true;
        }
    }


    public function attributeLabels() {
        return ['password'=>'Password'];
    }

        public function register()
    {
        $user = new Users;
        $userValues = [
            'email'=>$this->email,
            'status'=>$this->account_type,
            'password'=> self::hashPassword($this->password)
                ];
        
        $profileValues = ['title'=>$this->title,'firstname'=>$this->firstname, 'lastname'=>$this->lastname];
        
        $user->setAttributes($userValues);
        if($user->save())
        {
            $profile = new Profile;
            $profileValues = array_merge($profileValues, ['user_id'=>$user->getPrimaryKey()]);
            $profile->setAttributes($profileValues);
            if($profile->save())
            {
                $this->id = $profile->getPrimaryKey();//store new id
                return true;
            }
        }else
        {
            $this->addErrors($user->getErrors());
        }
        
        return false;
        
    }
    
    public function updateAccount(Users $user, Profile $profile)
    {
        if(!$this->validate())
        {
            return false;
        }
        $emailChanged = ( trim($this->email) != trim($user->email));
        $passwordChanged = !empty($this->password);
        $profile->firstname = $this->firstname;
        $profile->lastname = $this->lastname;
        $profile->title = $this->title;
        
        $user->email = $this->email;
        $this->emailChanged($emailChanged);
        $this->passwordUpdate($passwordChanged);
        return $profile->save() && $user->save();
    }
    
    public function passwordUpdate($passwordChanged = false, $password = null, Users $user = null)
    {
        if($passwordChanged)
        {
            //update the users password if provided
            $user->password = $this->hashPassword($password);
        }
    }


    /**
     * What should happen if email is updated as well.
     * Should a verification email be sent
     * @param bool $emailChanged
     */
    protected function emailChanged($emailChanged = false)
    {
        if($emailChanged)
        {
            
        }
    }
    /**
     * hash password
     * @param type $string
     * @return type
     */
    public static function hashPassword($string)
    {
        return md5($string);
    }
    
    public function getUsername()
    {
        return $this->email;
    }
    
    public function saveNewPassword()
    {
        $user = Users::findOne(['email'=>$this->email]);
        if($user instanceof Users)
        {
            $user->password_hash = self::hashPassword($this->new_password);
            $user->password_reset_token = md5(str_shuffle('QjSH496pcT5CEbzjD'));
            $user->save(false);
           if($this->sendEmail($this->email, $user->password_reset_token))
            return true;
        }
    }
    
    public function sendEmail($email = '', $token = '')
    {
        return app()->mailer->compose('newpassword', compact('token','email'))->setFrom(['admin@alodiahomes.com'=>'AlodiaHomes'])
                ->setTo($email)->setSubject('Password reset link')->send();
    }
    
    public function validateToken($email = '', $token = '')
    {
        $user = Users::findOne(
            [
                'password_reset_token' => (string) $token,
                'email'=> $email
            ]);
        if(!$user instanceof Users){
            return false;
        }
            $user->password = $user->password_hash;
            $user->password_hash = $user->password_reset_token = '';
            $user->save(false);
        return TRUE;
    }
}
