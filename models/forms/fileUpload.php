<?php
namespace app\models\forms;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\base\Model;

use app\components\fileUploadValidator;

use app\models\Users;
use app\models\Files;
use app\models\FilesUsage;
use app\models\Albums;

class fileUpload extends Model {
    
    public $fileValidator;
    public $userId = 0;
    public $response = []; //any messages
    public $files = [];
    public $key;
    public $model='';


    public function rules()
    {
        return [
            [['text','permissions'], 'required'],
        ];
    }
    
    protected function convertFiles($model)
    {
        echo '<hr/>';
        $this->key = 'files';
        $name= $_FILES[$model]['name'][$this->key];
        $type= $_FILES[$model]['type'][$this->key];
        $error= $_FILES[$model]['error'][$this->key];
        $size = $_FILES[$model]['size'][$this->key];
        $tmp_name = $_FILES[$model]['tmp_name'][$this->key];
                
        $this->files = compact('name','type','error','size','tmp_name');
    }

    
    public function validateFiles(array $config = [])
    {
        try{
            $this->fileValidator = new fileUploadValidator();
            return $this->fileValidator->checkFiles($config);
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
        
        
    }
    
    public function uploadFiles(array $config = [], $key = 'files')
    {
        
        $success = $failed = [];
        $section = isset($config['section']) ? $config['section'] : 'p';
        $model = isset($config['model']) ? $config['model'] : 'fileUpload';
        $propertyId = isset($config['propertyId']) ? $config['propertyId'] : 0;
        $this->convertFiles($model);
        $albumName = '';//$config['albumName'];
        $dateTaken = time();//strtotime($config['dateTaken']);
        if(empty($section))
        {
            throw new \ErrorException('Image section not given');
        }
        $index = isset($config['key']) ? $config['key'] : $key;
       $destination = \Yii::getAlias('@webroot').'/images/property';
        $this->userId = $config['userId'];
        if(is_dir($destination) && is_writable($destination))
        {
            foreach($this->files["error"] as $key => $error)
            {
                if ($error !== UPLOAD_ERR_OK) {continue;}
                $name = preg_replace('/[^\da-z]/i', '', $this->files['name'][$key]);
                $tt = explode('.', $this->files['name'][$key]);
                $ext = end($tt);
                $filePath =  $destination.DIRECTORY_SEPARATOR.$name.'.'.strtolower($ext);

                if(move_uploaded_file($this->files['tmp_name'][$key], $filePath))
                {
                    $success[] = ['name'=>$this->files['name'][$key],'location'=>$filePath,
                        'size'=>$this->files['size'][$key],'type'=>$this->files['type'][$key]];
                }else{
                    $failed[] =  ['name'=>$this->files['name'][$key],'location'=>$filePath];
                }
            }

            $this->handleSuccessFile($success,$propertyId);
            $this->handleErrorFiles($failed);

        }else
        {
            $this->response[] = $destination . ' neither exist nor writable.';
        }
        
        return $this->response;
    }
    
    public function unlinkFiles($files)
    {
        
        foreach($files as $file)
        {
            if(!file_exists($file['location'])) continue;
            unlink($file['location']);
        }
    }


    public function handleSuccessFile($success = [],$propertyId = 0, $dateTaken =0)
    {
        
        if(count($success))
        {
            
            $userId = $this->userId;
           
            foreach($success as $key => $array)
            {
                $file = new Files();
                $values = ['name'=>$array['name'], 'location'=>$array['location'],'date_taken'=>$dateTaken,
                    'created_at'=>time(), 'size'=>$array['size'],'type'=>$array['type']];
                $file->setAttributes($values);
                if($file->save())
                {
                    
                    $this->saveMediaRelationship($file,$propertyId,'property');
                    
                }else
                {
                    
                    $this->response[]=$file->getErrors();
                    
                }
                
            }
            
          //  return true;
        }
        
    }
    
    /**
     * Saves the relationship between a file and an album
     * @param Files $file
     * @param int $section_id
     */
    public function saveMediaRelationShip(Files $file,$section_id = 0, $section = 'p',  $user = null)
    {
        $fileUsage = new FilesUsage();
        $userId = ($user instanceof Users) ? $user->getPrimaryKey() : $this->userId;
        $values = [
                    'files_id'=>$file->getPrimaryKey(),
                    'section' => $section,
                    'created_at' => time(),
                    'user_id' => $userId,
                    'section_id'=>$section_id
                ];
        $fileUsage->setAttributes($values);
        if($fileUsage->save())
        {

        }
    }


    /**
     * Create an album for logged in user
     * @param string $albumName
     * @return int album id or 0 if no album name is given
     */
    public function createAlbum($albumName,$user = null)
    {
        
        try {
            $userId = ($user instanceof Users) ? $user->getPrimaryKey() : $this->userId;
            if(!empty($albumName))
            {
                $album = new Albums;
                $album->setAttribute('name', $albumName);
                $album->setAttribute('user_id', $userId);
                $album->setAttribute('access_type', 0);
                $album->setAttribute('created_at', time());
                $album->setAttribute('notes', '');
                if($album->save())
                {
                    return $album;
                }else
                {
                    return $this->response = $album->getErrors();
                }
            }
            
        return 0;
        
        } catch (\Exception $ex) {
            
            return $ex->getMessage();
        }
    }


    public function handleErrorFiles($failed = [])
    {
          
        if(count($failed))
        {
            foreach($failed as $file)
            {
                
            }
            
            $this->response[] =  json_encode($failed);
        }

    }
}