<?php
namespace app\models\forms;

use Yii;
use app\models\Property;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SearchForm extends \yii\base\Model{
    
    public $searchterm = '';
    public $min_bedrooms = 0;
    public $max_bedroom = 6;


    public function rules()
    {
        parent::rules();
    }
    
    public function setSearchTerm($searchterm = '')
    {
        $this->searchterm = $searchterm;
    }
    
    public function getSearchTerm()
    {
        return $this->searchterm;
    }


    public function buildQuery()
    {
       
         return Property::find()->where(['like', 'address_line_1', $this->getSearchTerm()])
                 ->orWhere(['like', 'address_line_2', $this->getSearchTerm()])
                 ->orWhere(['like', 'post_code', $this->getSearchTerm()])
                 ->orWhere(['like', 'address_line_3', $this->getSearchTerm()])
                 ->andWhere(['deleted'=> Property::PROPERTY_MODE_SHOW]);                 
        
    }
    
    public function runQuery()
    {
         if(!$this->getSearchTerm()){
             throw new Exception('Search term not set');
         }
        return $this->buildQuery();
        //return Property::findBySql($sql);
        
    }
}
