<?php
namespace app\models\forms;

/*
 * For property upload
 */
use Yii;
use app\models\Property;
/**
 * Description of PropertyForm
 *
 * @author BarnabasU
 */
class PropertyForm extends \yii\base\Model {
    public $files;
    public $propertyId = 0;
    public $address_line_1;
    public $address_line_2;
    public $address_line_3;
    public $house_no;
    public $post_code;
    public $description;
    public $discount_price=0;
    public $price_frequency;
    public $price;
    public $bedrooms;
    public $let_type;// 1 room or 2 house let
    public $key_features;
    public $availability_date, $closing_date, $contact_tel,
            $contact_email,$minimum_tenancy, $pets_preference
            ,$smoking_preference;



    public function rules() {
        return [
            [['house_no','contact_email','address_line_1','address_line_2','address_line_3','availability_date',
                'minimum_tenancy','post_code','description'],'required'],
            [['post_code','description','price_frequency','price','bedrooms','let_type','key_features',
                'address_line_1','address_line_2','address_line_3','house_no','discount_price'
                ,'availability_date', 'closing_date', 'contact_tel',
            'contact_email','minimum_tenancy', 'pets_preference'
            ,'smoking_preference'], 'safe'],
            [['description','key_features'], 'string', 'max' => 200],
            [['contact_tel','contact_email'], 'string', 'max' => 250],
            [['minimum_tenancy','pets_preference','smoking_preference'], 'string', 'max' => 50]

            ];
    }
    
    public function attributeLabels() 
    {
        return [
            'files' => 'Property Images',
            'key_features' => 'Key Features',
            'price' => 'Rental price'
        ];
    }
    
    public function attributeHints() {
        return ['key_features'=>'Hint: 3 bedrooms | Large Kitchen'];
    }

    /**
     * Add a new property
     * @return boolean
     */
    public function addNewProperty(Property $property = null)
    {
        $property = ($property instanceof Property) ? $property : new Property();
        $property->deleted = Property::PROPERTY_MODE_EDIT;//put property in edit mode
        $property->attributes = $this->attributes;
        $property->user_id = $this->getUserId();
        
        if($property->save())
        {
            $this->propertyId = $property->getPrimaryKey();
            $this->addPictures($this->propertyId);
            return true;
            
        }  else
        {
            $this->addErrors($property->getErrors());
            return false;
        }
    }
    
     public function addPictures($propertyId, $key = 'files')
    {
        $fileupload = new fileUpload();
        $passed = $fileupload->validateFiles(['key'=>$key]);
        if($passed)
        {
            
            $response = $fileupload->uploadFiles(
                    [
                        'key'=>$key,
                        'userId'=> $this->getUserId(),
                        'albumName'=>'album_name',
                        'dateTaken'=> time(),
                        'model'=> 'PropertyForm',
                        'propertyId'=>$propertyId
                    ]
                    );
            
            if(count($response))
            {
              return $response;
            }
            
        }else{

            $this->addError('files', $fileupload->fileValidator->getFileErrors());   

        }
    }

    public static function getPriceFrequency()
    {
        return Property::getPriceFrequency();
    }
    
    public static function getLetTypes()
    {
        return Property::getLetTypes();
    }
    
    protected function getUserId()
    {
        return userId();
    }
    
    public function approvePropertyAd(Property $property)
    {
       return $property->goLiveMode(); 
    }
    
}
