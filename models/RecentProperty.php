<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recent_property".
 *
 * @property string $recent_id
 * @property string $user_id
 * @property string $property_id
 * @property integer $created_date
 * @property integer $deleted
 * @property string $Column 6
 */
class RecentProperty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recent_property';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'property_id', 'created_date'], 'required'],
            [['user_id', 'property_id', 'created_date', 'deleted'], 'integer'],
            [['Column 6'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'recent_id' => Yii::t('app', 'Recent ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'property_id' => Yii::t('app', 'Property ID'),
            'created_date' => Yii::t('app', 'Created Date'),
            'deleted' => Yii::t('app', 'Deleted'),
            'Column 6' => Yii::t('app', 'Column 6'),
        ];
    }
    
    public static function getRecent($userid = 0, $limit = 5)
    {
         return self::find()
            ->where(['deleted' => 0, 'user_id'=>$userid])
            ->orderBy('recent_id')
            ->limit($limit)
            ->all();
    }
    
    public function getProperty()
    {
        
        $property = Property::getProperty($this->property_id);
        return $property;
    }
    
    public static function addToHistory(Property $property)
    {
        
        if(self::isFirsttime($property))
        {
            self::addToViewedProperty($property);
            if(!isGuest())
            {//if logged in add to user db record
                $r = new RecentProperty();
                $r->property_id = $property->getPropertyId();
                $r->user_id = userId();
                $r->created_date = time();
                $r->save(false);
            }
            //update property viewed count
            $property->views = $property->views + 1;
            $property->save(false,['views']);
        }
    }
   
    public static function addToViewedProperty(Property $property)
    {
        $p = app()->getSession()->get('viewedProperties',[]);
        array_push($p,$property->getPropertyId());
        return app()->getSession()->set('viewedProperties',$p);

    }

        /**
     * First time viewed
     * @return boolean
     */
    private static function isFirsttime(Property $property)
    {
        $properties = self::getViewedProperties();
        if(in_array($property->getPropertyId(), $properties))
        {
            return false;
        }
        
        return true;
    }
    
    public static function getViewedProperties()
    {
        return app()->getSession()->get('viewedProperties',[]);
    }
}
