<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Messages;

/**
 * MessagesSearch represents the model behind the search form about `app\models\Messages`.
 */
class MessagesSearch extends Messages
{
//    public $deleted = 0;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_id', 'from', 'to', 'message_parent_id', 'created_date', 'read', 'deleted'], 'integer'],
            [['modified_date', 'content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function getMessages($params)
    {
        $query = Messages::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
                        
        $query->andFilterWhere([
            'to' => $params['to'],
            'deleted' => $params['deleted'],
            'hide_in_inbox' => $params['hide_in_inbox']
            
        ]);//getSentMessages

        return $dataProvider;
    }
    
    public function getSentMessages($params)
    {
        $query = Messages::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
                        
        $query->andFilterWhere([
            'from' => $params['from'],
            'deleted' => $params['deleted'],
            'hide_in_sent' => $params['hide_in_sent']
            
        ]);

        return $dataProvider;
    }
    
     public function search($params)
    {
        $query = Messages::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'message_id' => $this->message_id,
            'from' => $this->from,
            'to' => $this->to,
            'message_parent_id' => $this->message_parent_id,
//            'created_date' => $this->created_date,
            'modified_date' => $this->modified_date,
            'read' => $this->read,
            'deleted' => $this->deleted,
        ]);

        $query->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }

}
