<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property string $id
 * @property string $firstname
 * @property string $lastname
 * @property string $title
 * @property string $user_id
 *
 * @property User $user
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','firstname','lastname'], 'required'],
            [['user_id'], 'integer'],
            [['firstname', 'lastname','title'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'firstname' => Yii::t('app', 'Firstname'),
            'lastname' => Yii::t('app', 'Lastname'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
    
    /**
     * @inheritdoc
     * @return ProfileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProfileQuery(get_called_class());
    }
    
    /**
     * Return fullname
     * @return string
     */
    public function getFullName()
    {
        return $this->title.' '.$this->firstname .' '.$this->lastname;
    }
    
    public function getFirstname()
    {
        return $this->firstname;
    }
    
    public function getLastname()
    {
        return $this->lastname;
    }
    
    public function getTitle()
    {
        return $this->title;
    }
    
    public function getUserPage()
    {
        $url = \yii\helpers\Url::to(['account/id','id'=>$this->user_id]);
        return \yii\helpers\Html::a($this->getFullName(), $url);
    }
}
