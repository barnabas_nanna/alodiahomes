<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "faq".
 *
 * @property string $faq_id
 * @property string $question
 * @property string $answer
 * @property string $user_id
 * @property string $deleted
 * @property string $created_date
 * @property string $modified_date
 */
class Faq extends \yii\db\ActiveRecord
{
    
    const FAQ_DELETED = 2;
    const FAQ_SHOW= 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['answer'], 'string'],
            [['user_id', 'answer','deleted','question', 'created_date'], 'required'],
            [['user_id', 'deleted', 'created_date'], 'integer'],
            [['modified_date'], 'safe'],
            [['question'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'faq_id' => Yii::t('app', 'Faq ID'),
            'question' => Yii::t('app', 'Question'),
            'answer' => Yii::t('app', 'Answer'),
            'user_id' => Yii::t('app', 'User ID'),
            'deleted' => Yii::t('app', 'Deleted'),
            'created_date' => Yii::t('app', 'Created Date'),
            'modified_date' => Yii::t('app', 'Modified Date'),
        ];
    }
    
    public function beforeValidate()
    {
        $this->user_id = userId();
        $this->created_date = time();
        
        return parent::beforeValidate();
    }
    
    public function getQuestion()
    {
        return $this->question;
    }
    
    public function getAnswer()
    {
        return $this->answer;
    }
    
    public function getUser()
    {
        return $this->user_id;
    }
    
    public function isDeleted()
    {
        return $this->deleted == self::FAQ_DELETED;
    }
    
    public function isActive()
    {
        return !$this->isDeleted();
    }
}
