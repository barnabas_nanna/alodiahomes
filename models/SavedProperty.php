<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "saved_property".
 *
 * @property string $saved_id
 * @property string $user_id
 * @property string $property_id
 * @property integer $created_date
 * @property integer $deleted
 * @property string $Column 6
 */
class SavedProperty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'saved_property';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'property_id', 'created_date'], 'required'],
            [['user_id', 'property_id', 'created_date', 'deleted'], 'integer'],
            [['Column 6'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'saved_id' => Yii::t('app', 'Saved ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'property_id' => Yii::t('app', 'Property ID'),
            'created_date' => Yii::t('app', 'Created Date'),
            'deleted' => Yii::t('app', 'Deleted'),
            'Column 6' => Yii::t('app', 'Column 6'),
        ];
    }
    
    public static function deleteUserProperty($userId = 0, $propertyId = 0, $saveId = 0)
    {
        SavedProperty::deleteAll('user_id ='.$userId.' AND property_id ='. $propertyId.' AND saved_id != '. $saveId);
    }
    
    public static function getRecent($userid = 0, $limit = 5)
    {
         return self::find()
            ->where(['deleted' => 0, 'user_id'=>$userid])
            ->orderBy('saved_id')
            ->limit($limit)
            ->all();
    }
    
    public function getProperty()
    {
        
        $property = Property::getProperty($this->property_id);
        
        return $property;
    }
}
