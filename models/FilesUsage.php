<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "files_usage".
 *
 * @property string $id
 * @property string $files_id
 * @property string $section
 * @property string $section_id
 * @property string $created_at
 * @property string $modified_date
 *
 * @property Files $files
 */
class FilesUsage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files_usage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['files_id'], 'required'],
            [['files_id', 'section_id', 'created_at'], 'integer'],
            [['modified_date','section_id'], 'safe'],
            [['section'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'files_id' => Yii::t('app', 'Files ID'),
            'section' => Yii::t('app', 'Section'),
            'section_id' => Yii::t('app', 'Section ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'modified_date' => Yii::t('app', 'Modified Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasOne(Files::className(), ['file_id' => 'files_id']);
    }

    /**
     * @inheritdoc
     * @return FilesUsageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FilesUsageQuery(get_called_class());
    }
}
