<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_coordinates".
 *
 * @property string $id
 * @property string $query
 * @property string $result
 */
class SiteCoordinates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_coordinates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['result'], 'string'],
            [['query'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'query' => Yii::t('app', 'Query'),
            'result' => Yii::t('app', 'Result'),
        ];
    }
    
    public function saveCoords($query,$results){
        if(!SiteCoordinates::find("query='$query'")->count()){
            $SiteCoordinates = new SiteCoordinates;
            $SiteCoordinates->query = $query;
            $SiteCoordinates->result = serialize($results);
            $SiteCoordinates->save();
        }
    }
    
    /**
     * Fetches geocoding data from google or database. 
     * saves if not in db
     * @param string search query can be a postcode or address
     * @return array
     */
    public function getCoords($query){
        $coords = SiteCoordinates::findOne("query='$query'");
        if(!$coords){
            $coords = getPostcodeData($query);
            if(isset($coords['lng'])){
           $this->saveCoords($query, $coords);
           return $coords;
            }
        }else{//already in db
            return unserialize($coords->result);
        }
        
        return array();
    }
    
}
