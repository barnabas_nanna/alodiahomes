<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "files".
 *
 * @property string $file_id
 * @property string $name
 * @property string $size
 * @property integer $width
 * @property integer $height
 * @property string $location
 * @property integer $created_at
 * @property integer $modified_date
 * @property string $type
 *
 * @property FilesUsage[] $filesUsages
 */
class Files extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'size', 'width', 'height', 'location', 'created_at', 'modified_date', 'type'], 'safe'],
            [['width', 'height', 'created_at', 'modified_date','size'], 'integer'],
            [['name', 'type'], 'string', 'max' => 50],
            [['location'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file_id' => Yii::t('app', 'File ID'),
            'name' => Yii::t('app', 'Name'),
            'size' => Yii::t('app', 'Size'),
            'width' => Yii::t('app', 'Width'),
            'height' => Yii::t('app', 'Height'),
            'location' => Yii::t('app', 'Location'),
            'created_at' => Yii::t('app', 'Created At'),
            'modified_date' => Yii::t('app', 'Modified Date'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilesUsages()
    {
        return $this->hasMany(FilesUsage::className(), ['files_id' => 'file_id']);
    }

    /**
     * @inheritdoc
     * @return FilesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FilesQuery(get_called_class());
    }
}
