<?php

namespace app\models;

use Yii;
use app\models\MessagesSearch;

/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $modified_date
 *
 * @property Profile[] $profiles
 */
class Users extends \yii\db\ActiveRecord
{
    const ACCOUNT_TYPE_STUDENT = 1;
    const ACCOUNT_TYPE_LANDLORD = 2;
    const ACCOUNT_TYPE_ADMIN = 10;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           // [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [[ 'password', 'email', 'created_at'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['modified_date'], 'safe'],
            [['email'],'unique'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32]
        ];
    }
    
    public function beforeValidate() {
        if($this->isNewRecord)
        {
            $this->created_at = time();
        }
        $this->modified_date = time();
        
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'modified_date' => Yii::t('app', 'Modified Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }
    
    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
    
    public function isLandLord()
    {
        return (int)$this->status === self::ACCOUNT_TYPE_LANDLORD;
    }
    
    public function isStudent()
    {
        return $this->status === self::ACCOUNT_TYPE_STUDENT;
    }
    
     public function isAdmin()
    {
        return $this->status === self::ACCOUNT_TYPE_ADMIN;
    }
    
    public function getStatus()
    {
        return $this->status;
    }


    public function getEmail()
    {
        return $this->email;
    }
    
    public function MembershipDate($format = true)
    {
        return $format ? date('d / M / Y',$this->created_at) : $this->created_at;
    }


    /**
     * Get user messages
     * @param MessagesSearch $searchModel
     * @return type
     */
    public static function getMessages(MessagesSearch $searchModel)
    {
        return $searchModel->getMessages(['to' => userId(), 'deleted' => Messages::DISPLAY, 'hide_in_inbox'=>0]);
    }
    
    /**
     * Get user messages
     * @param MessagesSearch $searchModel
     * @return type
     */
    public static function getSentMessages(MessagesSearch $searchModel)
    {
        return $searchModel->getSentMessages(['from' => userId(), 'deleted' => Messages::DISPLAY, 'hide_in_sent'=>0]);
    }
    
    public function getStatusname()
    {
        $status = [self::ACCOUNT_TYPE_STUDENT => 'Student', self::ACCOUNT_TYPE_ADMIN => 'Admin', self::ACCOUNT_TYPE_LANDLORD=>'Landlord'];
        return isset($status[$this->status]) ? $status[$this->status] : $this->status;
    }
    
    
    
    
}
