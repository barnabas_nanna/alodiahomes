<?php
/* 
 * Helper functions
 */

/**
 * Returns the user identity class
 * @return \app\models\User
 * @throws \Exception
 */
function identity()
{
    if(\Yii::$app->user->isGuest)
    {
        goHome();//throw new \Exception('User not logged in');
    }
    
    return Yii::$app->user->identity;
}

/**
 * 
 * @return \yii\web\Application
 */
function app()
{
    return \Yii::$app;
}

function userId()
{
    try
    {
         return (int)identity()->id;
         
    } catch (\Exception $ex) {
        
        //throw new \Exception('User not logged in');
        goHome();
    }
   
}

function goHome()
{
    \Yii::$app->response->redirect(["site/login"]);
}

function isStudent()
{
    if(isGuest())
    {
        return false;
    }
    
    return identity()->isStudent();
}

function isAdmin()
{
    if(isGuest())
    {
        return false;
    }
    
    return identity()->isAdmin();
}

function db($sql = null)
{
    $connection = Yii::$app->getDb();
    return $connection->createCommand($sql);
}

function isLandLord()
{
    if(isGuest()){
        return false;
    }else
    {
        return identity()->landlord;
    }
}

function isGuest()
{
    return Yii::$app->user->isGuest;
}

function lang($message='',$app='app'){
    return \Yii::t($app,$message);
}

function hasFlash($key = '')
{
    if(empty($key)) return false;
    return Yii::$app->getSession()->hasFlash($key);
}

function getFlash($key = '', $default = '' , $delete = true)
{
    if(empty($key)) return $default;
    return Yii::$app->getSession()->getFlash($key,$default,$delete);
}

function setFlash($key = '', $default = '' , $delete = true)
{
    if(!empty($key)) 
    Yii::$app->getSession()->setFlash($key,$default,$delete);
}

function getPostcodeData($postcode, $r = false) {
    $trim_postcode = str_replace(' ', '', preg_replace("/[^a-zA-Z0-9\s]/", "", strtolower($postcode)));

    $q_center = 'http://maps.googleapis.com/maps/api/geocode/json?region=gb&address=' . $trim_postcode . '&sensor=false';
    $json_center = file_get_contents($q_center); //$q_center);
    $details_center = json_decode($json_center, TRUE);

    if ($details_center['status'] == 'OVER_QUERY_LIMIT')
        return array();

    if ($r)
        return $details_center;

    if (isset($details_center['results']) && isset($details_center['results'][0])) {

        $center_lat = null;
        $center_lng = null;
        if (isset($details_center['results'][0]['geometry']['location'])) {
            $center_lat = $details_center['results'][0]['geometry']['location']['lat'];
            $center_lng = $details_center['results'][0]['geometry']['location']['lng'];
        }

        $boundsNE = null;
        $boundsSW = null;
        if (isset($details_center['results'][0]['geometry']['bounds'])) {
            $boundsNE = $details_center['results'][0]['geometry']['bounds']['northeast'];
            $boundsSW = $details_center['results'][0]['geometry']['bounds']['southwest'];
        }

        $status = isset($details_center['results'][0]['status']) ? $details_center['results'][0]['status'] : '';
        $types = isset($details_center['results'][0]['types']) ? $details_center['results'][0]['types'] : '';

        return array(
            'postcode' => $trim_postcode,
            'lat' => $center_lat,
            'lng' => $center_lng,
            'NE' => $boundsNE,
            'SW' => $boundsSW,
            'status' => $status,
            'types' => $types
        );
    } else {

        return array();
    }
}

/**
 * 
 * @param type $ne NE coordinates
 * @param type $sw SW coordinates
 * @return array center coordinates
 */
function calCenter($ne, $sw) {
    if (!is_array($ne) OR !is_array($sw))
        return array();
    $r = array();

    $x1 = $ne['lat'];
    $y1 = $ne['lng'];

    $x2 = $sw['lat'];
    $y2 = $sw['lng'];

    $mtx = ($x1 + $x2) / 2;
    $mty = ($y1 + $y1) / 2;

    $mbx = ($x1 + $x2) / 2;
    $mby = ($y2 + $y2) / 2;

    $mlx = ($x2 + $x2) / 2;
    $mly = ($y1 + $y2) / 2;

    $mrx = ($x1 + $x1) / 2;
    $mry = ($y1 + $y2) / 2;

    $r['topCenter'] = array($mtx, $mty);
    $r['bottomCenter'] = array($mbx, $mby);
    $r['leftCenter'] = array($mlx, $mly);
    $r['rightCenter'] = array($mrx, $mry);
    $r['northeast'] = array($x1, $y1);
    $r['southwest'] = array($x2, $y2);

    return $r;
}

/**
 * Removes special characters from a string
 * @param string $string string you want cleaned
 * @return string clean string
 */
function cleanForImageUpload($string, $replaceSpaces = true) {
    if ($replaceSpaces)
        $string = str_replace(" ", "-", trim($string)); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^_A-Za-z0-9\-\.]/', '', $string); // Removes special chars.
    $string = preg_replace('/_+/', '_', $string); // Replaces multiple hyphens with single one.
    return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}

function getfileType($finfo, $TEMPFILE) {
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    return $mime = finfo_file($finfo, $TEMPFILE);
}
