<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\AccountForm */
/* @var $form ActiveForm */

$this->title = 'Register';

?>
<div class="site-register">
    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php $form = ActiveForm::begin(['layout'=>'horizontal']); ?>
    <?php echo $form->errorSummary($model);?>
    <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'firstname') ?>
        <?= $form->field($model, 'lastname') ?>
        <?= $form->field($model, 'password')->passwordInput(); ?>
        <?= $form->field($model, 'confirm_password')->passwordInput(); ?>
    <?php
     echo $form->field($model, 'account_type')
        ->dropDownList(
                app\models\forms\AccountForm::getPublicAccountTypes(), // Flat array ('id'=>'label')
            ['prompt'=>'--Select Account type--', 
                'required'=>'required']    // options
        );
    ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-theme-bg btn-lg']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-register -->
