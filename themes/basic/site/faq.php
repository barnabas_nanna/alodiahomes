<?php
use yii\helpers\Html;
use app\models\Faq;
use yii\widgets\LinkPager;

$this->title = lang('Frequently asked Questions');
?>
<h1><?= Html::encode($this->title) ?></h1>
<div class="divide30"></div>
<div class="row">
<div class="col-md-12">
    <div class="panel-group" id="accordion">
        <?php 
        foreach($questions  as $faq):
            $id = $faq->getPrimaryKey();
        ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $id;?>" aria-expanded="false" class="collapsed">
                        <?php echo $faq->getQuestion();?>
                    </a>
                </h4>
            </div>
            <div id="collapseOne<?php echo $id;?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                <div class="panel-body">
                    <?php echo $faq->getAnswer();?>
                </div>
            </div>
        </div>
        <?php endforeach;?>
        
    </div>
</div><!--collapse col-->
</div>

<?php
// display pagination
echo LinkPager::widget([
    'pagination' => $pages,
]);
?>
