<?php
use yii\helpers\Html;
use app\models\Cms;
use yii\helpers\Url;

$topBlock = Cms::getBlock('about_us_top_content');
$leftBlock = Cms::getBlock('about_us_bottom_left_content');
$rightBlock = Cms::getBlock('about_us_bottom_right_content');

?>

<?php $this->title='About AlodiaHomes';?>
<h1><?= Html::encode($topBlock->title) ?></h1>
   <div class='divide5'></div>


<div class="row">
                        <div class="col-sm-12">
                           <!-- <h4>About AlodiaHomes</h4> -->
                            <p class="paraText">
                                <?php echo $topBlock->getContent()?>
                            </p>
                        </div>
                    </div>
                    <div class="divide30"></div>
                    <div class="row">
                        <div class="col-sm-5">
                            <h4><?php echo $leftBlock->getTitle();?></h4>
                            <div class='divide5'></div>
                            <p class="paraText">
                                <?php echo $leftBlock->getContent();?>
                            </p>
                            <?php if(isGuest()):?>
                            <a href='<?php echo Url::to(['/site/register'])?>'>
                            <button class="border-white btn-lg">Sign up Landlords</button>
                            </a>
                            <?php endif;?>
                        </div>
                        <div class="col-sm-5 col-sm-offset-1">
                            <h4><?php echo $rightBlock->getTitle();?></h4>
                            <div class='divide5'></div>

                            <p class="paraText">
                                <?php echo $rightBlock->getContent();?>
                            </p>
                            <?php if(isGuest()):?>
                            <a href='<?php echo Url::to(['/site/register'])?>'>
                            <button class="border-white btn-lg btn-link">Sign up Students</button>
                            </a>
                            <?php endif;?>
                        </div>
                    </div><!--1/2 row end-->