<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Reset Password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin([
        'id' => 'reset-form',
        'layout' => 'horizontal'
    ]); ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'new_password')->passwordInput() ?>
    
    <?= $form->field($model, 'confirm_password')->passwordInput() ?>

    <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-theme-bg btn-lg', 'name' => 'login-button']) ?>
            <?= Html::a('Sign Up', yii\helpers\Url::to('register'), ['class' => 'btn btn-default btn-lg', 'name' => 'login-button']) ?>
        
    </div>

    <?php ActiveForm::end(); ?>
</div>
