<?php
/* @var $this yii\web\View */
/* @user $user app\models\Users */

use yii\bootstrap\Tabs;
?>

<?php
echo $this->render('partials/_profile', compact('user', 'profile'));
?>

<div class='row'>
    <div class='col-xs-12' id='accountProfile'>

        <?php if($user->isLandlord()):?>
            <h3><?php echo lang('User Properties') ?></h3>
            <div class='divide20'></div>

            <?php
            echo $this->render('partials/_user_properties', array('user'=>$user,
                'savedProperties'=>$userProperties));
            ?>
        <?php endif?>
            
        <div class='divide20'></div>
        

    </div>
</div>

