<?php
/* @var $user  */

?>
<h1><?php echo $profile->getFullName();?></h1>
<div class='divide20'></div>
<table class='table table-bordered table-responsive'>
    <tr><td>Title:</td><td><?php echo $profile->getTitle()?></td></tr>
    <tr><td>Firstname:</td><td><?php echo $profile->getFirstname()?></td></tr>
    <tr><td>Lastname:</td><td><?php echo $profile->getLastname()?></td></tr>
    <tr><td>Email:</td><td><?php echo $user->getEmail()?></td></tr>
    <tr><td>Member since:</td><td><?php echo $user->MembershipDate(true)?></td></tr>
</table>