<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\PropertyForm */
/* @var $form ActiveForm */
?>


<div class="row segment">
    <?php $form = ActiveForm::begin(); ?>
    <?php echo $form->errorSummary($model);?>
    <?= $form->field($model, 'house_no') ?>
    <?= $form->field($model, 'address_line_1') ?>
    <?= $form->field($model, 'address_line_2') ?>
    <?= $form->field($model, 'address_line_3'); ?>
    <?= $form->field($model, 'post_code'); ?>
    <?= $form->field($model, 'bedrooms'); ?>
    <?= $form->field($model, 'price'); ?>
    <?php //echo $form->field($model, 'discount_price'); ?>
    <?php
     echo $form->field($model, 'price_frequency')
        ->dropDownList(
                app\models\forms\PropertyForm::getPriceFrequency(), // Flat array ('id'=>'label')
            ['prompt'=>'--Select Price frequency--', 
                'required'=>'required']    // options
        );
    ?>
    
    <?php
     echo $form->field($model, 'let_type')
        ->dropDownList(
                app\models\forms\PropertyForm::getLetTypes(), // Flat array ('id'=>'label')
            ['prompt'=>'--Select Let type--', 
                'required'=>'required']    // options
        );
    ?>
    <?= $form->field($model, 'description')->textarea(); ?>

</div>

<div class="images segments">
    
</div>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>