<?php

/* @var $messages \app\model\Messages */

use \app\models\Messages;
?>
<table class='table table-bordered table-hover table-responsive onclick-table messages-table'>
    <thead><tr><th>Title</th><th>Message</th></tr></thead>
    <tbody>
<?php
foreach($recentMessages as $messages):
?>
        <tr onclick="location.href='<?php  echo $messages->getLink();?>'">
        <td><?php echo $messages->title?></td>
        <td><?php echo $messages->content?></td>
    </tr>
<?php
endforeach;
?>

    <tbody>
</table>

