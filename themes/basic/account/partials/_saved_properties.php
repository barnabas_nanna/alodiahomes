<?php
/* 
 * Recently saved properties
 */
?>

<table class='table table-condensed table-striped table-bordered table-hover onclick-table table-responsive properties-table'>
    <thead>
        <tr><th>Image</th><th>Address</th><th>Rent</th></tr>
    </thead>
    <tbody>
    <?php
    foreach($savedProperties as $property):
        if( !($property = $property->getProperty()) instanceof app\models\Property)
        continue;
        ?>
        <tr>
            <td><a href='<?php echo $property->getPropertyLink();?>'><img src="<?php echo $property->getMainImage();?>"/></a></td>
            <td><?php echo $property->getDisplayAddress();?></td>
            <td><?php echo $property->getDisplayPrice();?></td>
        </tr>
    <?php
    endforeach;
    ?>

    </tbody>
</table>
