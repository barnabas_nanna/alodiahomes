<?php
/**
 * Reently viewed properties
 */
?>
<table class='table table-condensed table-striped table-bordered table-hover table-responsive properties-table onclick-table'>
    <thead>
        <tr><th>Image</th><th>Address</th><th>Rent</th></tr>
    </thead>
    <tbody>
    <?php
    foreach($recentProperties as $property):
        if( !($property = $property->getProperty()) instanceof app\models\Property)
        continue;
        ?>
        <tr onclick="location.href='<?php echo $property->getPropertyLink();?>'">
            <td><img src="<?php echo $property->getMainImage();?>"/></td>
            <td><?php echo $property->getDisplayAddress();?></td>
            <td><?php echo $property->getDisplayPrice();?></td>
        </tr>
    <?php
    endforeach;
    ?>

    </tbody>
</table>