<?php
/* @var $this yii\web\View */
use yii\bootstrap\Tabs;
?>

<?php

echo $this->render('partials/_profile', compact('user','profile'));

echo Tabs::widget([
    'items' => [ 
        [
            'active' => true,
            'label' => 'Property Upload',
            'content' => $this->render('partials/_property_upload',['model'=>$propertyForm]),
            'headerOptions' => [],
            'options' => ['id' => 'myveryownID'],            
        ],
        [
            'label' => 'Messages',
            'items' => [
                 [
                     'label' => 'Unread Messages',
                     'content' => $this->render('partials/_unread_messages'),
                 ],
                 [
                     'label' => 'Read Messages',
                     'content' => $this->render('partials/_read_messages'),
                 ],
            ],
        ],
    ],
]);
