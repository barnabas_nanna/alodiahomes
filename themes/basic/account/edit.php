<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\AccountForm */
/* @var $form ActiveForm */

$this->title = lang('Edit details');

?>
<div class="site-register">
    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php $form = ActiveForm::begin(); ?>
    <?php echo $form->errorSummary($model);?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'firstname') ?>
        <?= $form->field($model, 'lastname') ?>
        <?= $form->field($model, 'password')->passwordInput(); ?>
        <?= $form->field($model, 'confirm_password')->passwordInput(); ?>    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Update Details'), ['class' => 'btn btn-theme-bg btn-lg']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-register -->
