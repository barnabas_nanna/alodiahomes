<?php
/* @var $this yii\web\View */
use yii\bootstrap\Tabs;
?>

<?php
echo $this->render('partials/_profile', compact('user','profile'));
?>

<div class='row'>
    <div class='col-xs-12' id='accountProfile'>
        
<h3><?php echo lang('Saved Properties')?></h3>
<div class='divide20'></div>

<?php
echo $this->render('partials/_saved_properties', compact('user','savedProperties'));
?>
<div class='divide20'></div>
<h3><?php echo lang('New Messages')?></h3>
<div class='divide20'></div>

<?php
echo $this->render('partials/_recent_messages', compact('user','recentMessages'));
?>
<div class='divide20'></div>
<h3><?php echo lang('Recently Viewed')?></h3>
<div class='divide20'></div>
<?php
echo $this->render('partials/_recent_properties', compact('user','recentProperties'));
?>

</div>
</div>

