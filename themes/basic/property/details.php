<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Property;

/* @var $this yii\web\View */
/* @var $model app\models\forms\PropertyForm */
/* @var $form ActiveForm */
/* @var $property app\models\Property */

?>
<?php
$this->title = $property->getDisplayAddress();

echo $this->render('partials/_more_details', ['property'=>$property,'propertyImages'=>$propertyImages]);
