<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Property;

/* @var $this yii\web\View */
/* @var $model app\models\forms\PropertyForm */
/* @var $form ActiveForm */
/* @var $property app\models\Property */

?>
<div class="account-property">
<?php
$this->title = $property->getDisplayAddress();
?>
<h3 class="address"><?= Html::encode($this->title) ?></h3>
   <div class="row">
       <?php $firstImage = current($propertyImages);?>
       <?php
         $src = Property::getImageSrc($firstImage['location']);
         echo '<p><img id="firstImage" src="'.$src.'"/></p>';
       ?>
   </div> 
    <div class="row">
        <?php foreach($propertyImages as $image):?>
       <?php
            $src = Property::getImageSrc($image['location']);
            echo '<img class="smallimages" src="'.$src.'"/>';
       ?>
       <?php endforeach;?>
    </div>
<div class="divide20"></div>
    <div class="row">
      <h4>Bedrooms</h4>
      <p><?php echo $property->bedrooms;?> bedrooms</p>
      <h4>Rent</h4>
      <p><?php echo $property->getPrice();?> <?php echo $property->getPriceFrequencyName(true);?></p>
      <h4>Let Type</h4>
      <p><?php echo $property->getLetTypeName();?></p>
      <h4>Description</h4>
      <p><?php echo $property->description;?>  </p>
    </div>

</div><!-- account-property -->


<script type="text/javascript">
    window.onload = function(){
    $('.smallimages').click(function(){
        $('#firstImage').attr('src', $(this).attr('src'));
    });
    };
    </script>