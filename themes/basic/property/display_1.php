            <div class="row">
                <div class="col-md-8 margin30">
                    <div class="row margin30">
                    
                    
                        <div class="col-md-4">
                            <form method="post" action="#">
                                <select class="form-control">
                                    <option>Sorting by Rent</option>
                                    <option>Sort by rent: low to high</option>
                                    <option>Sort by rent: high to low</option>
                                </select>
                            </form><!--shorting form end-->
                        </div> 
                    </div><!--row top end-->
                    
                    <div class="row">
                        <div class="col-md-6 margin40">
                            <div class="product-col">
                                <a href="/property/details/?id=3">
                                    <img src ="/images/dummy-property.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="product-desc-sh">
                                    <span>£50 p/w</span>
                                    <a href="/property/details/?id=3" class="btn btn-default">View Property</a>
                                </div>                     
                            </div><!--products show col end-->
                        </div><!--col sm 6-->
                        <div class="col-md-6 margin40">
                            <div class="product-col">
                                <!--<span class="sale-label">Sale</span>-->
                                <a href="/property/details/?id=3">
                                    <img src ="/images/dummy-property.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="product-desc-sh">
                                    <span>£50 p/w</span>
                                    <a href="/property/details/?id=3" class="btn btn-default">View Property</a>
                                </div>                     
                            </div><!--products show col end-->
                        </div><!--col sm 6-->
                        <div class="col-md-6 margin40">
                            <div class="product-col">
                                <a href="/property/details/?id=3">
                                    <img src ="/images/dummy-property.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="product-desc-sh">
                                    <span>£50 p/w</span>
                                    <a href="/property/details/?id=3" class="btn btn-default">View Property</a>
                                </div>                     
                            </div><!--products show col end-->
                        </div><!--col sm 6-->
                        <div class="col-md-6 margin40">
                            <div class="product-col">
                                <a href="/property/details/?id=3">
                                    <img src ="/images/dummy-property.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="product-desc-sh">
                                    <span>£100p/w</span>
                                    <a href="/property/details/?id=3" class="btn btn-default">View Property</a>
                                </div>                     
                            </div><!--products show col end-->
                        </div><!--col sm 6-->
                        <div class="col-md-6 margin40">
                            <div class="product-col">
                                <a href="/property/details/?id=3">
                                    <img src ="/images/dummy-property.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="product-desc-sh">
                                    <span>£125 p/w</span>
                                    <a href="/property/details/?id=3" class="btn btn-default">View Property</a>
                                </div>                     
                            </div><!--products show col end-->
                        </div><!--col sm 6-->
                        <div class="col-md-6 margin40">
                            <div class="product-col">
                                <!--<span class="sale-label">Sale</span>-->
                                <a href="/property/details/?id=3">
                                    <img src ="/images/dummy-property.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="product-desc-sh">
                                    <span>£399p/w</span>
                                    <a href="/property/details/?id=3" class="btn btn-default">View Property</a>
                                </div>                     
                            </div><!--products show col end-->
                        </div><!--col sm 6-->
                        <div class="col-md-6 margin40">
                            <div class="product-col">
                                <a href="/property/details/?id=3">
                                    <img src ="/images/dummy-property.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="product-desc-sh">
                                    <span>£100 p/w</span>
                                    <a href="/property/details/?id=3" class="btn btn-default">View Property</a>
                                </div>                     
                            </div><!--products show col end-->
                        </div><!--col sm 6-->
                        <div class="col-md-6 margin40">
                            <div class="product-col">
                                <!--<span class="sale-label">Sale</span>-->
                                <a href="/property/details/?id=3">
                                    <img src ="/images/dummy-property.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="product-desc-sh">
                                    <span>£149 p/w</span>
                                    <a href="/property/details/?id=3" class="btn btn-default">View Property</a>
                                </div>                     
                            </div><!--products show col end-->
                        </div><!--col sm 6-->
                    </div><!--products row-->
                    <ul class="pagination">
                        <li>
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true">«</span>
                            </a>
                        </li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li>
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true">»</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!--products list columns end-->
                <div class="col-md-3 col-md-offset-1 shop-sidebar">
                    <div class="sidebar-box margin40">
                        <h4>Search</h4>
                        <form role="form" class="search-widget">
                            <input type="text" class="form-control">
                            <i class="fa fa-search"></i>
                        </form>
                    </div><!--sidebar-box-->

                    
                    <div class="sidebar-box margin40">
                        <h4>Most Viewed</h4>
                       <ul class="list-unstyled popular-post">
                            <li>
                                <div class="popular-img">
                                    <a href="#"> <img src="/images/dummy-property.jpg" class="img-responsive" alt=""></a>
                                </div>
                                <div class="popular-desc">
                                    
                                    <h6>£150 p/w</h6>
                                    <h5> <a href="#">view</a></h5>
                                </div>
                            </li>
                            <li>
                                <div class="popular-img">
                                    <a href="#"> <img src="/images/dummy-property.jpg" class="img-responsive" alt=""></a>
                                </div>
                                <div class="popular-desc">
                                    
                                    <h6>£100 p/w</h6>
                                    <h5> <a href="#">view</a></h5>
                                </div>
                            </li>
                            <li>
                                <div class="popular-img">
                                    <a href="#"> <img src="/images/dummy-property.jpg" class="img-responsive" alt=""></a>
                                </div>
                                <div class="popular-desc">
                                    
                                    <h6>£50 p/w</h6>
                                    <h5> <a href="#">view</a></h5>
                                </div>
                            </li>
                        </ul>
                    </div><!--sidebar-box-->
                     <div class="sidebar-box margin40">
                        <h4>Recently Viewed</h4>
                       <ul class="list-unstyled popular-post">
                            <li>
                                <div class="popular-img">
                                    <a href="#"> <img src="/images/dummy-property.jpg" class="img-responsive" alt=""></a>
                                </div>
                                <div class="popular-desc">
                                    
                                    <h6>£150 p/w</h6>
                                    <h5> <a href="#">view</a></h5>
                                </div>
                            </li>
                            <li>
                                <div class="popular-img">
                                    <a href="#"> <img src="/images/dummy-property.jpg" class="img-responsive" alt=""></a>
                                </div>
                                <div class="popular-desc">
                                    
                                    <h6>£100 p/w</h6>
                                    <h5> <a href="#">view</a></h5>
                                </div>
                            </li>
                            <li>
                                <div class="popular-img">
                                    <a href="#"> <img src="/images/dummy-property.jpg" class="img-responsive" alt=""></a>
                                </div>
                                <div class="popular-desc">
                                    
                                    <h6>£50 p/w</h6>
                                    <h5> <a href="#">view</a></h5>
                                </div>
                            </li>
                        </ul>
                    </div><!--sidebar-box-->
                   
                </div><!--sidebar-->
            </div>
