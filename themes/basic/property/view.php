<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Property;

/* @var $this yii\web\View */
/* @var $model app\models\Property */

$this->title = $model->getDisplayAddress();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Properties'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class='divide20'></div>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->property_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->property_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('app', 'Index'), ['index', 'id' => $model->property_id], ['class' => 'btn btn-theme-bg btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'View Ad'), ['/property/details', 'id' => $model->property_id], ['class' => 'btn btn-success btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'property_id',
            //'longitude',
            //'latitude',
            'house_no',
            'address_line_1',
            'address_line_2',
            'address_line_3',
            'post_code',
            //'coordinates',
            'description:ntext',
            'user_id',
            'let_type',
            'price_frequency',
            'bedrooms',
            'price',
            'discount_price',
            'key_features:ntext',
            'availability_date',
            'closing_date',
            'contact_email:email',
            'contact_tel',
            'minimum_tenancy',
            'pets_preference',
            'smoking_preference',
            'deleted:deleted',
            'views',


        ],
    ]) ?>
    
   

</div>

<div class='row'>
    <div class="col-xs-9">
    <?php
    $propertyImages = $model->getImages();
    $firstImage = current($propertyImages);?>
           <?php
             $src = Property::getImageSrc($firstImage['location']);
             echo '<p><img id="firstImage" src="'.$src.'"/></p>';
           ?> 
</div>
</div>

<div class="row">
    <div class="col-xs-12">
        <?php foreach($propertyImages as $image):?>
        <?php
            $src = Property::getImageSrc($image['location']);
            echo '<img class="smallimages" src="'.$src.'"/>';
       ?>
       <?php endforeach;?>
     </div>
</div>

<script type="text/javascript">
    window.onload = function(){
    $('.smallimages').click(function(){
        $('#firstImage').attr('src', $(this).attr('src'));
    });
    };
    </script>
