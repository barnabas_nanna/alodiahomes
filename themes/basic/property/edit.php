<?php
use yii\helpers\Html;
$this->title = 'Edit property';
?>
<h1><?= Html::encode($this->title) ?></h1>
<?php
echo $this->render('partials/_property_upload', ['model'=>$propertyForm]);

