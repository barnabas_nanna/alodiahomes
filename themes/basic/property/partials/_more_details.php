<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Property;

?>

<div class="row account-property">
    
    <div class="col-xs-9">
        <h3 class="address"><?= Html::encode($property->getDisplayAddress()) ?>
            <br/><span class="price"><?php echo $property->getDisplayPrice();?></span>
        </h3>
        <div>
        <?php $firstImage = current($propertyImages);?>
           <?php
             $src = Property::getImageSrc($firstImage['location']);
             echo '<p><img id="firstImage" src="'.$src.'"/></p>';
           ?> 
        </div>
        
    </div>
    
    <div class="col-xs-3">
        <div class="property-actions">
            <div class="save-property" data-propertyid="<?php echo $property->getPropertyId()?>">
            <i class="glyphicon glyphicon-star"></i> 
                <span>Save Property</span>
            </div>
            <div class="email-landlord" data-propertyid="<?php echo $property->getPropertyId();?>">
                <i class="glyphicon glyphicon-envelope"></i>
                <span>Email Landlord</span>
            </div>
            <div class="report-property" data-propertyid="<?php echo $property->getPropertyId();?>">
                <i class="glyphicon glyphicon-envelope"></i>
                <span>Report Property</span>
            </div>
            
<!--            <i class="glyphicon glyphicon-print"></i> 
            <span style="font-family:'Raleway',sans-serif; font-size:15px;">Print page</span>
            <br/>-->
            <br/>
            <button class="btn btn-default btn-lg" style="color:#fff;"
                    onclick='$("#contactdetails").toggle()'>Request details</button>
            <div id='contactdetails' class="no-display">
                <?php if(!empty($property->contact_email)):?>
                <div>
                    <h4><?php echo $property->getAttributeLabel('contact_email');?></h4>
                <?php echo $property->contact_email; ?>
                </div>
                 <?php endif;?>
                <?php if(!empty($property->contact_tel)):?>
                <div>
                    <h4><?php echo $property->getAttributeLabel('contact_tel');?></h4>
                <?php echo $property->contact_tel; ?>
                </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <?php foreach($propertyImages as $image):?>
        <?php
            $src = Property::getImageSrc($image['location']);
            echo '<img class="smallimages" src="'.$src.'"/>';
       ?>
       <?php endforeach;?>
     </div>
</div>
    
<div class="divide20"></div>

<div id='property-details'>
    
<div class="row">
    <div class="col-xs-9">
        <h4><?php echo $property->getAttributeLabel('price');?></h4>
            <?php echo $property->getDisplayPrice(); ?> (<?php echo $property->getPriceFrequencyName(true); ?>)
    </div>
</div>

<div class="row">
    <div class="col-xs-9">
        <h4><?php echo $property->getAttributeLabel('key_features');?></h4>
        <?php 
            $html = new \app\components\listSorter($property->getKeyFeatures(), 3, 'list');
            echo $html->getHtml();
         ?>        
    </div>
</div>

<div class="row">
    <div class="col-xs-9">
        <h4><?php echo $property->getAttributeLabel('availability_date');?></h4>
            <?php echo $property->availability_date;?>  
    </div>
</div>

<div class="row">
    <div class="col-xs-9">
        <h4><?php echo $property->getAttributeLabel('pets_preference');?></h4>
            <?php echo $property->pets_preference;?>  
    </div>
</div>

<div class="row">
    <div class="col-xs-9">
        <h4><?php echo $property->getAttributeLabel('smoking_preference');?></h4>
            <?php echo $property->availability_date;?>  
    </div>
</div>

<div class="row">
    <div class="col-xs-9">
        <h4><?php echo $property->getAttributeLabel('let_type');?></h4>
            <?php echo $property->getLetTypeName(); ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-9">
        <h4><?php echo $property->getAttributeLabel('minimum_tenancy');?></h4>
            <?php echo $property->minimum_tenancy;?>  
    </div>
</div>

<div class='row'>
    <div class="col-xs-9">
        <h4><?php echo $property->getAttributeLabel('description');?></h4>
        <?php echo nl2br($property->description); ?> 
    </div>    
</div>
</div>

<script type="text/javascript">
    window.onload = function(){
    $('.smallimages').click(function(){
        $('#firstImage').attr('src', $(this).attr('src'));
    });
    };
    </script>