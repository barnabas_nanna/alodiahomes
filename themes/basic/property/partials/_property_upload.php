<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\Property;

/* @var $this yii\web\View */
/* @var $model app\models\forms\PropertyForm */
/* @var $form ActiveForm */
/* @var $property app\models\Property*/
?>
<div class="divide20"></div>
<?php if($property instanceof Property):?>
<div class="row">
    <div class="col-xs-9">
        <div>
        <?php
        $propertyImages = $property->getImages();
        $firstImage = current($propertyImages);?>
           <?php
             $src = Property::getImageSrc($firstImage['location']);
             echo '<p><img id="firstImage" src="'.$src.'"/></p>';
           ?> 
        </div>
        
        <?php
        foreach($propertyImages as $image):?>
        <?php
            $src = Property::getImageSrc($image['location']);
            echo '<img class="smallimages" src="'.$src.'"/>';
       ?>
       <?php endforeach;?>
     </div>
</div>
<?php endif; ?>
<div class="divide20"></div>
<div class="row segment">
    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
    <?php echo $form->errorSummary($model);?>
    <?= $form->field($model, 'files[]')->fileInput(['multiple'=>true])?>
    <?= $form->field($model, 'house_no') ?>
    <?= $form->field($model, 'address_line_1') ?>
    <?= $form->field($model, 'address_line_2') ?>
    <?= $form->field($model, 'address_line_3'); ?>
    <?= $form->field($model, 'post_code'); ?>
    <?= $form->field($model, 'bedrooms'); ?>
    <?= $form->field($model, 'price'); ?>
    <?php //echo $form->field($model, 'discount_price'); ?>
    <?php
     echo $form->field($model, 'price_frequency')
        ->dropDownList(
                app\models\forms\PropertyForm::getPriceFrequency(), // Flat array ('id'=>'label')
            ['prompt'=>'--Select Price frequency--', 
                'required'=>'required']    // options
        );
    ?>
    
    <?php
     echo $form->field($model, 'let_type')
        ->dropDownList(
                app\models\forms\PropertyForm::getLetTypes(), // Flat array ('id'=>'label')
            ['prompt'=>'--Select Let type--', 
                'required'=>'required']    // options
        );
    ?>
    <?= $form->field($model, 'key_features')->textarea(); ?>
    <?= $form->field($model, 'description')->textarea(); ?>
    
    <?= $form->field($model, 'availability_date')->widget(\yii\jui\DatePicker::className(),['dateFormat'=>'yyyy-M-dd']); ?>
    <?php //$form->field($model, 'closing_date'); ?>
    <?= $form->field($model, 'contact_email'); ?>
    <?= $form->field($model, 'contact_tel'); ?>
    <?= $form->field($model, 'minimum_tenancy'); ?>
    <?= $form->field($model, 'pets_preference'); ?>
    <?= $form->field($model, 'smoking_preference'); ?>
    

</div>

<div class="images segments">
    
</div>
    
<div class="form-group">
   <?= Html::submitButton(
                empty($property) ? lang('Submit Property Listing') :
            lang('Update Property Listing'), 
            ['class' => 'btn btn-theme-bg btn-lg'])
        ?>
    <?php if($property instanceof Property): ?>
    <?= Html::a(lang('Delete Property'), Url::to(['/property/deleteproperty','id'=>$property->getPropertyId()]) ,
                ['class' => 'btn btn-danger btn-lg']) ?>
    <?php endif;?>
        
</div>

<?php ActiveForm::end(); ?>