<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Property;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\forms\PropertyForm */
/* @var $form ActiveForm */
/* @var $property app\models\Property */
$this->title = 'Property Preview';

echo $this->render('partials/_more_details', ['property'=>$property,'propertyImages'=>$propertyImages]);
?>

    <div class="divide20"></div>

<?php $form = ActiveForm::begin(); ?>

<div class="row">
<div class="form-group">
        <?= Html::submitButton(
                !$property->isLive() ? lang('Approve Property Listing') :
            lang('Update Property Listing'), 
            ['class' => !$property->isLive() ? 'btn btn-theme-bg btn-lg' : 'btn btn-primary btn-lg'])
        ?>
        <?= Html::a(lang('Edit Property'), Url::to(['/property/add','id'=>$property->getPropertyId()]) ,
                ['class' => 'btn btn-success btn-lg']) ?>
        
        <?php
        if($property->isLive())
        {
           echo Html::a(lang('View Property'), $property->getPropertyLink() , ['class' => 'btn btn-theme-bg btn-lg']);
        }
        ?>
    </div>
    
</div>

<?php ActiveForm::end(); ?>
