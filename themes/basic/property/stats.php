<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = lang('Properties Stats');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class='divide20'></div>

    <p>
        <?= Html::a(Yii::t('app', 'index'), ['index'], ['class' => 'btn btn-theme-bg']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'property_id',
            [
                'header'=>'Property Address',
                'attribute' => 'DisplayAddress',
            ],
            'views',
            'price',
            // 'post_code',
            // 'coordinates',
            // 'description',
            // 'user_id',
            // 'price_frequency',
            // 'bedrooms',
            // 'price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
