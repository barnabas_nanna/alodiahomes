<?php
use yii\helpers\Html;
$this->title = 'Add property';
?>
<h1><?= Html::encode($this->title) ?></h1>
<?php
echo $this->render('partials/_property_upload', ['model'=>$propertyForm,'property'=>$property]);

?>
<script type="text/javascript">
    window.onload = function(){
    $('.smallimages').click(function(){
        $('#firstImage').attr('src', $(this).attr('src'));
    });
    };
    </script>

