<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $property app\models\Property */

$this->title = 'Contact';
$this->params['breadcrumbs'][] = lang('Contact Landlord');
$this->title = lang('Contact Landlord');
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php 
        if(hasFlash('messageNotSent')):?>
            <div class="alert alert-error">
                <?php echo getFlash('messageNotSent', lang('Message not sent.')) ?>
            </div>
    <?php endif;?>

    
    <?php if(isset($property) && $property instanceof app\models\Property):?>
    <div id='message-property'>
        <p>Property:</p>
        <h3 class="address">
            <?= Html::encode($property->getDisplayAddress()); ?>
            <span class="price"><?php echo $property->getDisplayPrice();?></span>
        </h3>
    </div>
    <?php endif;?>

    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['id' => 'message-form']); ?>
                <?= $form->errorSummary($modelForm); ?>
                <?= $form->field($modelForm, 'title') ?>
                <?= $form->field($modelForm, 'content')->textArea(['rows' => 6]) ?>
                <?= $form->field($modelForm, 'property_id')->hiddenInput()->label(false); ?>
                
                <div class="form-group">
                    <?= Html::submitButton(lang('Send Message'), ['class' => 'btn btn-theme-bg btn-lg', 'name' => 'contact-button']) ?>
                    <?= Html::a(lang('Back'), $property->getPropertyLink(), ['style'=>'color:white','class' => 'btn btn-default btn-lg', 'name' => 'contact-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
