<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
$this->title = lang('RE:').$model->title;
?>
<div class="site-contact">
    <h1><?=lang('RE:'). Html::encode($model->title) ?></h1>
    <div class="divide20"></div>

    <?php if (Yii::$app->session->hasFlash('messageNotSent')): ?>

    <div class="alert alert-error">
        An Error occurred. Do try again.
    </div>

    <?php endif; ?>

    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['id' => 'message-form']); ?>
                <?= $form->errorSummary($modelForm); ?>
                <?= $form->field($modelForm, 'title') ?>
                <?= $form->field($modelForm, 'content')->textArea(['rows' => 6]) ?>
                <?= $form->field($modelForm, 'message_parent_id')->hiddenInput()->label(false); ?>
                
                <div class="form-group">
                    <?= Html::submitButton(lang('Send Message'), ['class' => 'btn btn-theme-bg btn-lg', 'name' => 'contact-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
