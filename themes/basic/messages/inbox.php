<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MessagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Messages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="messages-index">
    
    <?php if (Yii::$app->session->hasFlash('messageSent')): ?>

    <div class="alert alert-success">
        Thank you for contacting us. We will respond to you as soon as possible.
    </div>

    <p>
        Note that if you turn on the Yii debugger, you should be able
        to view the mail message on the mail panel of the debugger.
        <?php if (Yii::$app->mailer->useFileTransport): ?>
        Because the application is in development mode, the email is not sent but saved as
        a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
        Please configure the <code>useFileTransport</code> property of the <code>mail</code>
        application component to be false to enable email sending.
        <?php endif; ?>
    </p>

    <?php endif ?>

    <h1><?= Html::encode($this->title) ?></h1>
            
    <div class='row'>
        <div class='col-xs-12'>
            <?= Html::a('Sent Messages', yii\helpers\Url::to('/messages/sent'),['class'=>'btn btn-primary pull-right']) ?>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            [
              'header'=>'From',
              'attribute' => 'sender.fullname',
            ],
            'title',
            'created_date:date:Date',//'attribute:format:Label',
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{read}&nbsp;&nbsp;&nbsp;&nbsp;{delete}',
                'buttons'=>[
                    'read' => function ($url, $model, $key) {
                            /** @var ActionColumn $column */
                            $url = yii\helpers\Url::toRoute(['read','id'=>$model->getPrimaryKey()]);
                            return Html::a(lang('view'), $url, [
                                'class'=>'btn btn-default btn-link',
                                    'title' => Yii::t('yii', 'View'),
                            ]);
                        },
          
                ]
                
            ],
        ],
    ]); 
    
    ?>

</div>
