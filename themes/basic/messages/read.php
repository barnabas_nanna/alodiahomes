<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \Yii;

/* @var $this yii\web\View */
/* @var $model app\models\Messages */

$this->title = lang('Read Message').'('.$model->title.')';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?php echo $model->title;?></h1>
<div class='divide20'></div>
<div class="messages-view">
   
    <p><span class='purple-text'>From:</span> <?php echo $model->getSender()->getFullName();?></p>
    <p><span class='purple-text'>Date:</span> <?php echo Yii::$app->formatter->asDate($model->created_date, 'long');?></p>
    <p><span class='purple-text'>Message:</span></p>
    <p><?php echo Yii::$app->formatter->format($model->content, 'ntext');?></p>
    
    <div class="form-group">
         <?= Html::a('Reply', \yii\helpers\Url::to(['reply','id'=>$model->getPrimaryKey()]),['class'=>'btn btn-theme-bg btn-lg']) ?>
    </div>
    
</div>
