<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MessagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Messages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="messages-index">

    <h1><?= Html::encode($this->title) ?></h1>
            
    <div class='row'>
        <div class='col-xs-12'>
            <?= Html::a('Inbox', yii\helpers\Url::to('inbox'),['class'=>'btn btn-primary pull-right']) ?>
        </div>
    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        
        'filterModel' => null,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            [
              'header'=>'To',
              'attribute' => 'receiver.fullname',
                
            ],
            'title',
            'created_date:date:Date',//'attribute:format:Label',
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{read}',
                'buttons'=>[
                    'read' => function ($url, $model, $key) {
                            /** @var ActionColumn $column */
                            $url = yii\helpers\Url::toRoute(['read','id'=>$model->getPrimaryKey()]);
                            return Html::a('view', $url, [
                                'class'=>'btn btn-default btn-link',
                                    'title' => Yii::t('yii', 'View'),
                            ]);
                        },
          
                ]
                
            ],
        ],
    ]); 
    
    ?>

</div>