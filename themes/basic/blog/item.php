<?php
use app\models\Blog;
/* @var $blog app\models\Blog */
?>

<div class='row'>
    <div class="col-xs-12">
    <?php
        echo $blog->toDisplayString(true);
    ?>
    </div>
</div>

