<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Block;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php
    echo $this->render('_head');
    ?>
    <link href="/css/style.css" rel="stylesheet">
    <?php if(!isGuest()):?>
        <?php if(identity()->student): ?>
        <link href="/css/student.css" rel="stylesheet">
        <?php elseif(identity()->landlord): ?>
        <link href="/css/landlord.css" rel="stylesheet">
        <?php endif;?>
    <?php endif;?>
    
    
</head>
<body>
<?php
    echo $this->render('_header');
    echo $this->render('_nav');
    ?>
<?php $this->beginBody() ?>
    <div class="divide40"></div>
    <div class="container"> 
        <?= $content ?>
    </div>
    <div class="divide60"></div>

    
<?php
echo $this->render('_footer');
?>

<?php $this->endBody() ?>
     <script src="/assan/js/jquery-migrate.min.js"></script> 
        <!--bootstrap js plugin-->
        <script src="/assan/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
       <script src="/assan/js/jquery.easing.1.3.min.js" type="text/javascript"></script>
        <!--sticky header-->
        <script type="text/javascript" src="/assan/js/jquery.sticky.js"></script>
        <!--flex slider plugin-->
        <script src="/assan/js/jquery.flexslider-min.js" type="text/javascript"></script>
        <!--parallax background plugin-->
        <script src="/assan/js/jquery.stellar.min.js" type="text/javascript"></script>
        <!--digit countdown plugin-->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
        <!--digit countdown plugin-->
        <script src="/assan/js/jquery.counterup.min.js" type="text/javascript"></script>
        <!--on scroll animation-->
        <script src="/assan/js/wow.min.js" type="text/javascript"></script> 
        <!--owl carousel slider-->
        <script src="/assan/js/owl.carousel.min.js" type="text/javascript"></script>
        <!--popup js-->
        <script src="/assan/js/jquery.magnific-popup.min.js" type="text/javascript"></script>
        <!--you tube player-->
        <script src="/assan/js/jquery.mb.YTPlayer.min.js" type="text/javascript"></script>        
        <!--customizable plugin edit according to your needs-->
        <script src="/assan/js/custom.js" type="text/javascript"></script>

        <!--revolution slider plugins-->
        <script src="/assan/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
        <script src="/assan/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
        <script src="/assan/js/revolution-custom.js" type="text/javascript"></script>
        <!--cube portfolio plugin-->
        <script src="/assan/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
        <script src="/assan/js/cube-portfolio.js" type="text/javascript"></script>
        <script src="/js/app.js" type="text/javascript"></script>
 
</body>
</html>
<?php $this->endPage() ?>