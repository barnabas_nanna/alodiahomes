<?php 
use yii\helpers\Url;
$this->beginContent('@app/themes/basic/layouts/main.php');
?>

<div class="row">
   <div class="col-sm-3 margin40" style='margin-top:80px'>
                    <ul class="list-unstyled side-nav">
                        <?php if(isLandlord()): ?>
                        <li><a href="<?php echo Url::to(['/property/add']);?>"><i class="fa fa-plus"></i>Add Property</a></li>
                        <li><a href="<?php echo Url::to(['/property/properties']);?>"><i class="fa fa-building"></i>My Properties</a></li>
                        <?php endif;?>
                        <?php if(!isGuest()): ?>
                        <li><a href="<?php echo Url::to(['/property/saved']);?>"><i class="fa fa-cloud"></i>Saved Properties</a></li>
                        <li><a href="<?php echo Url::to(['/messages']);?>"><i class="fa fa-inbox"></i>Messages</a></li>
                        <li><a href="<?php echo Url::to(['/account/edit']);?>"><i class="fa fa-user"></i>Update Details</a></li>
                        <?php endif;?>
                        <li><a href="<?php echo Url::to(['/property/recent']);?>"><i class="fa fa-eye"></i>Recently Viewed</a></li>
                        <li><a href="<?php echo Url::to(['/search']);?>"><i class="fa fa-search"></i>Search</a></li>
                    </ul>
                </div><!--sidebar col end-->
  <div class="col-md-9">
      <div class="row">
        <?php 
        foreach(app()->getSession()->getAllFlashes() as $key => $message):
            if(!in_array($key, ['error','success','info','danger'])) continue;
            echo '<div class="alert alert-'.$key.'">'.$message.'</div>';
        endforeach;
        ?>
      </div>
    <?= $content; ?>
  </div>
</div>
<?php $this->endContent(); ?>