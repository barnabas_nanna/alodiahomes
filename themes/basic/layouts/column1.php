<?php $this->beginContent('@app/themes/basic/layouts/main.php'); ?>

<div class="row row-centered">
  <div class="col-md-9 col-centered">
          <?= $content; ?>
</div>
</div>
<?php $this->endContent(); ?>