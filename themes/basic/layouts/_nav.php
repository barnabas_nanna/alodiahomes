  <?php 
  use yii\helpers\Url;
  ?>
  
  
  
  
  
  <div class="navbar navbar-default navbar-static-top yamm sticky" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo Yii::$app->homeUrl;?>"><img src="/images/alodiaLogo.png" style="width:180px; margin-top:-40px; padding-bottom:10px;" alt="ALODIAHOMES"></a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                             <a href="<?php echo Yii::$app->homeUrl;?>">Home</a> 
                        </li>
                        <?php if(!Yii::$app->user->isGuest):?>
                        <li class="">
                             <a href="<?php echo Url::to(['/account']);?>">Account</a> 
                        </li>
                        <?php endif;?>
                        <li class="">
                             <a href="<?php echo Url::to(['/site/about']);?>">About Us</a> 
                        </li>
                        <li class="">
                             <a href="<?php echo Url::toRoute(['/property/display']);?>">Properties</a> 
                        </li>
<!--                        <li class="">
                             <a href="/site/pricing">Pricing</a> 
                        </li>-->
                        <li class="">
                             <a href="<?php echo Url::to(['/site/faq']);?>">FAQ</a> 
                        </li>
                        <li class="">
                             <a href="<?php echo Url::to(['/blog']);?>">Blog</a> 
                        </li>
                        <li class="">
                             <a href="<?php echo Url::to(['/site/contact']);?>">Contact Us</a> 
                        </li>
                         <?php if(!isGuest() && identity()->isAdmin()):// && Yii::$app->user->isGuest):?>
                        <li class="dropdown">
                            <a href="#" class=" dropdown-toggle" data-toggle="dropdown"><i class="fa fa-lock"></i> Admin</a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-login-box animated fadeInUp">
                                <ul>
                                    <li><a href='<?php echo Url::to(['/cms/index']);?>'>CMS</a></li>
                                    <li><a href='<?php echo Url::to(['/faq/index']);?>'>FAQ</a></li>
                                    <li><a href='<?php echo Url::to(['/search/index']);?>'>Search</a></li>
                                    <li><a href='<?php echo Url::to(['/profile/index']);?>'>Users</a></li>
                                    <li><a href='<?php echo Url::to(['/messages/index']);?>'>Messages</a></li>
                                    <li><a href='<?php echo Url::to(['/property/index']);?>'>Property</a></li>
                                    <li><a href='<?php echo Url::to(['/category/index']);?>'>Blog Categories</a></li>
                                    <li><a href='<?php echo Url::to(['/blog/index']);?>'>Blog</a></li>
                                </ul>
                            </div>
                        </li> <!--menu login li end here-->
                        <?php endif;?>
                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--container-->
        </div><!--navbar-default-->