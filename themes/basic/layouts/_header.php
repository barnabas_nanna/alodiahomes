      <div class="top-bar-dark">            
            <div class="container">
                <div class="row">
 <?php 
  use yii\helpers\Url;
?>
  <div class="col-sm-4">
                        <ul class="list-inline socials-simple">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                    <p class="scrollspy-example"><?php echo app\models\Cms::getBlock('header_scrolling_message')->getContent(true)?></p>
                    </div>
                    <div class="col-sm-8 text-right">
                        <ul class="list-inline top-dark-right">                      
                            <li class="hidden-sm hidden-xs"><i class="fa fa-envelope"></i>support@alodiahomes.com</li>
                            <li class="hidden-sm hidden-xs"><i class="fa fa-phone"></i> 0121 1870 453</li>
                           <?php if(Yii::$app->user->isGuest):?>
                            <li><a href="<?php echo Url::to(['/site/login']);?>"><i class="fa fa-lock"></i>Login</a></li>
                             <li><a href="<?php echo Url::to(['/site/register']);?>"><i class="fa fa-user"></i> Sign Up</a></li>
                            <?php else:?>
                               <li><a href="<?php echo Url::to(['/site/logout']);?>"><i class="fa fa-lock"></i>Logout</a></li>
                            <?php endif;?>
                            <li><a class="topbar-icons" href="#"><span><i class="fa fa-search top-search"></i></span></a></li>
                        </ul>
                         <div class="search">
                            <form role="form">
                                <input type="text" class="form-control" autocomplete="off" placeholder="Property search">
                                <span class="search-close"><i class="fa fa-times"></i></span>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div><!--top-bar-dark end here-->