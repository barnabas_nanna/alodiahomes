<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use app\components\SearchWidget;

$this->title = lang('Search Result');
?>
<h1><?= Html::encode($this->title) ?></h1>
<div class='divide20'></div>
<div class="row">
<div class="col-md-8 margin30">
<div class="row" id="property-list">
    <?php
        $p = [];
        foreach($properties as $property):
            if(!$property instanceof app\models\Property)
                continue;
            $p[] = $property;
        endforeach;
    ?>
    
    <?php 
            $html = new \app\components\listSorter($p, 2, 'list');
            echo $html->getHtml();
    ?> 
   
    
</div>
    <!--products row-->

<?php
// display pagination
echo LinkPager::widget([
    'pagination' => $pages,
]);
?>

</div><!--products list columns end-->
<div class="col-md-3 col-md-offset-1 shop-sidebar">
<?php
    echo SearchWidget::widget();
  ?>


<div class="sidebar-box margin40">
    <h4>Most Viewed</h4>
   <ul class="list-unstyled popular-post">
       <?php 
       foreach($mostViewProperties as $property):
        if( !($property instanceof app\models\Property))
        continue;
        ?>
        <li>
            <div class="popular-img">
                <a href="<?php echo $property->getPropertyLink();?>"> 
                <img src="<?php echo $property->getMainImage();?>" class="img-responsive"/>
                </a>
            </div>
            <div class="popular-desc">

                <h6><?php echo $property->getDisplayPrice();?></h6>
                <h5> <a href="<?php echo $property->getPropertyLink();?>">view</a></h5>
            </div>
        </li>
        <?php endforeach;?>
    </ul>
</div><!--sidebar-box-->
 <div class="sidebar-box margin40">
    <h4>Recently Viewed</h4>
   <ul class="list-unstyled popular-post">
       <?php 
       foreach($recentProperties as $property):
       if( !($property = $property->getProperty()) instanceof app\models\Property)
       continue;
        ?>
        <li>
            <div class="popular-img">
                <a href="<?php echo $property->getPropertyLink();?>"> 
                <img src="<?php echo $property->getMainImage();?>" class="img-responsive"/>
                </a>
            </div>
            <div class="popular-desc">

                <h6><?php echo $property->getDisplayPrice();?></h6>
                <h5> <a href="<?php echo $property->getPropertyLink();?>">view</a></h5>
            </div>
        </li>
        <?php endforeach;?>
    </ul>
</div><!--sidebar-box-->

</div><!--sidebar-->

</div>
