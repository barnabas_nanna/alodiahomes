<?php

use yii\db\Schema;
use yii\db\Migration;

class m150702_163403_alter_table_fileusage extends Migration
{
    public function up()
    {
        $sql ="ALTER TABLE `files_usage`
	CHANGE COLUMN `album_id` `section_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0' AFTER `section`;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m150702_163403_alter_table_fileusage cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
