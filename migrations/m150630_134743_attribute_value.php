<?php

use yii\db\Schema;
use yii\db\Migration;

class m150630_134743_attribute_value extends Migration
{
    public function up()
    {
        $sql="CREATE TABLE `attribute_value` (
	`value_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`attribute_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`element_id` INT(10) UNSIGNED NOT NULL,
	`deleted` TINYINT(4) NULL DEFAULT '0' COMMENT '2 - deleted, 1 - hidden , 0 - show',
	`value` TEXT NULL,
	`section` TINYINT(4) NULL DEFAULT NULL COMMENT '1 - user_id, 2 -property_id',
	PRIMARY KEY (`value_id`)
        )
        COMMENT='attribute values'
        ENGINE=InnoDB
        ;
        ";
        $this->execute($sql);
        
        
    }

    public function down()
    {
        echo "m150630_134743_attribute_value cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
