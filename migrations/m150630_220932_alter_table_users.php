<?php

use yii\db\Schema;
use yii\db\Migration;

class m150630_220932_alter_table_users extends Migration
{
    public function up()
    {
        $sql ="ALTER TABLE `user`
	ADD UNIQUE INDEX `email` (`email`);
";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m150630_220932_alter_table_users cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
