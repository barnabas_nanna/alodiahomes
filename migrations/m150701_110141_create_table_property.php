<?php

use yii\db\Schema;
use yii\db\Migration;

class m150701_110141_create_table_property extends Migration
{
    public function up()
    {
        $sql="CREATE TABLE `property` (
	`property_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`house_no` INT(10) UNSIGNED NOT NULL,
	`address_line_1` VARCHAR(250) NOT NULL,
	`address_line_2` VARCHAR(250) NOT NULL,
	`address_line_3` VARCHAR(250) NOT NULL,
	`post_code` VARCHAR(250) NOT NULL,
	`coordinates` VARCHAR(250) NOT NULL,
	`description` TEXT NOT NULL,
	`user_id` INT(11) NOT NULL,
	`let_type` INT(11) NOT NULL,
	`price_frequency` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '1 per week, 2 per month',
	`bedrooms` TINYINT(4) NOT NULL DEFAULT '1' COMMENT 'how many bedrooms',
	`price` FLOAT(10,2) NOT NULL,
	PRIMARY KEY (`property_id`),
	INDEX `house_no` (`house_no`),
	INDEX `address_line_1` (`address_line_1`),
	INDEX `address_line_2` (`address_line_2`),
	INDEX `address_line_3` (`address_line_3`),
	INDEX `post_code` (`post_code`),
	INDEX `coordinates` (`coordinates`),
	INDEX `price_frequency` (`price_frequency`),
	INDEX `bedrooms` (`bedrooms`),
	INDEX `price` (`price`))
COMMENT='Properties'
ENGINE=InnoDB
;
";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m150701_110141_create_table_property cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
