<?php

use yii\db\Schema;
use yii\db\Migration;

class m150630_130517_user_table extends Migration
{
    public function up()
    {
        $sql ="CREATE TABLE `user` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`auth_key` VARCHAR(32) NOT NULL COLLATE 'utf8_unicode_ci',
	`password_hash` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`password` VARCHAR(255) NOT NULL,
	`password_reset_token` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`email` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`status` SMALLINT(6) NOT NULL DEFAULT '10',
	`created_at` BIGINT(20) NOT NULL,
	`updated_at` BIGINT(20) NOT NULL,
	`modified_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m150630_130517_user_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
