<?php

use yii\db\Schema;
use yii\db\Migration;

class m150630_153525_attribute_value_fk extends Migration
{
    public function up()
    {
        $sql="ALTER TABLE `attribute_value`
	ADD CONSTRAINT `FK_attribute_value_attributes` FOREIGN KEY (`attribute_id`) 
        REFERENCES `attributes` (`attribute_id`) ON UPDATE NO ACTION;
        ";
        
        $this->execute($sql);

    }

    public function down()
    {
        echo "m150630_153525_attribute_value_fk cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
