<?php

use yii\db\Schema;
use yii\db\Migration;

class m150701_184131_alter_table_property extends Migration
{
    public function up()
    {
        $sql="ALTER TABLE `property`
	ADD COLUMN `discount_price` FLOAT(10,2) NOT NULL AFTER `price`;
";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m150701_184131_alter_table_property cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
