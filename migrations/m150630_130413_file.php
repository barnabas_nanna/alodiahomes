<?php

use yii\db\Schema;
use yii\db\Migration;

class m150630_130413_file extends Migration
{
    public function up()
    {
        $sql="CREATE TABLE `files_usage` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`files_id` BIGINT(20) UNSIGNED NOT NULL,
	`section` VARCHAR(20) NOT NULL DEFAULT '',
	`album_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
	`created_at` BIGINT(20) NULL DEFAULT NULL,
	`modified_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	INDEX `files_id` (`files_id`, `section`, `album_id`, `created_at`),
	INDEX `FK_files_usage_albums` (`album_id`),
	CONSTRAINT `FK_files_usage_files` FOREIGN KEY (`files_id`) REFERENCES `files` (`file_id`) ON UPDATE CASCADE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m150630_130413_file cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
