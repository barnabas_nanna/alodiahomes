<?php

use yii\db\Schema;
use yii\db\Migration;

class m150630_190428_alter_table_attributes extends Migration
{
    public function up()
    {
        $sql ="ALTER TABLE `attributes`
	ADD COLUMN `admin_name` VARCHAR(250) NOT NULL AFTER `name`,
	ADD UNIQUE INDEX `admin_name` (`admin_name`);
        ";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m150630_190428_alter_table_attributes cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
