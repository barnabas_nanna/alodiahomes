<?php

use yii\db\Schema;
use yii\db\Migration;

class m150630_130853_album_table extends Migration
{
    public function up()
    {
        $sql="CREATE TABLE `albums` (
	`album_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` BIGINT(20) UNSIGNED NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	`created_at` BIGINT(20) NOT NULL,
	`modified_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`notes` TEXT NOT NULL,
	`access_type` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'who can view album',
	PRIMARY KEY (`album_id`),
	INDEX `user_id` (`user_id`, `name`, `created_at`),
	INDEX `access_type` (`access_type`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=InnoDB
        ;
        ";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m150630_130853_album_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
