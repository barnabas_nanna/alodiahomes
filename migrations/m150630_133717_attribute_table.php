<?php

use yii\db\Schema;
use yii\db\Migration;

class m150630_133717_attribute_table extends Migration
{
    public function up()
    {
        $sql="CREATE TABLE `attributes` (
	`attribute_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`sector` INT(11) NULL DEFAULT NULL,
	`description` VARCHAR(250) NULL DEFAULT NULL,
	`name` VARCHAR(250) NULL DEFAULT NULL,
	PRIMARY KEY (`attribute_id`)
        )
        COMMENT='Attributes of elements in application'
        ENGINE=InnoDB
        ;
        ";
        
        $this->execute($sql);
    }

    public function down()
    {
        echo "m150630_133717_attribute_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
