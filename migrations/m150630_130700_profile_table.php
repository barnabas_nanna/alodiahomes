<?php

use yii\db\Schema;
use yii\db\Migration;

class m150630_130700_profile_table extends Migration
{
    public function up()
    {
        $sql="CREATE TABLE `profile` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`firstname` VARCHAR(30) NOT NULL DEFAULT '',
	`lastname` VARCHAR(30) NOT NULL DEFAULT '',
	`user_id` BIGINT(20) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `user_id` (`user_id`),
	INDEX `firstname` (`firstname`, `lastname`),
	CONSTRAINT `profile_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=3
;
";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m150630_130700_profile_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
