<?php

use yii\db\Schema;
use yii\db\Migration;

class m150630_105537_files extends Migration
{
    public function up()
    {
        $sql="CREATE TABLE `files` (
	`file_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL,
	`size` VARCHAR(255) NOT NULL,
	`width` INT(11) NOT NULL,
	`height` INT(11) NOT NULL,
	`location` VARCHAR(255) NOT NULL,
	`created_at` INT(11) NOT NULL,
	`modified_date` INT(11) NOT NULL,
	`type` VARCHAR(50) NOT NULL,
	PRIMARY KEY (`file_id`),
	INDEX `name` (`name`),
	INDEX `location` (`location`, `type`, `size`, `width`, `height`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m150630_105537_files cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
